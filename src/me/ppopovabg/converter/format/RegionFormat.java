package me.ppopovabg.converter.format;

import java.io.File;
import java.nio.ByteBuffer;

import org.bukkit.Bukkit;

public final class RegionFormat {
	public static final int _1_16_DATAVERSION = 2520;
	public static final int _1_15_DATAVERSION = 2230;
	public static final int _1_16_BIOME_SIZE = 1024;
	public static final int BLOCK_ARR_SIZE = 4096;
	public static final int NIBBLE_ARR_SIZE = BLOCK_ARR_SIZE / 2;
	
	public static final byte LDB_SUBCHUNK_PREFIX = '/';
	public static final byte LDB_TILES_PREFIX = '1';
	public static final byte LDB_ENTITIES_PREFIX = 50;
	
	private RegionFormat()
	{
	}

	public static int getLevelDBIndex(int x, int y, int z) {
		return x * 256 + z * 16 + y;
	}

	public static byte[] getLevelDBKey(int chunkX, int chunkZ, byte prefix) {
        return new byte[] {
            (byte) (chunkX & 0xff),
            (byte) ((chunkX >>> 8) & 0xff),
            (byte) ((chunkX >>> 16) & 0xff),
            (byte) ((chunkX >>> 24) & 0xff),
            (byte) (chunkZ & 0xff),
            (byte) ((chunkZ >>> 8) & 0xff),
            (byte) ((chunkZ >>> 16) & 0xff),
            (byte) ((chunkZ >>> 24) & 0xff),
            prefix
        };
    }
	
	public static byte[] getLevelDBKey(int chunkX, int chunkZ, int yBase, byte prefix) {
        return new byte[] {
            (byte) (chunkX & 0xff),
            (byte) ((chunkX >>> 8) & 0xff),
            (byte) ((chunkX >>> 16) & 0xff),
            (byte) ((chunkX >>> 24) & 0xff),
            (byte) (chunkZ & 0xff),
            (byte) ((chunkZ >>> 8) & 0xff),
            (byte) ((chunkZ >>> 16) & 0xff),
            (byte) ((chunkZ >>> 24) & 0xff),
            prefix,
            (byte) yBase
        };
    }
	
	public static int getPaletteIndex(final int dataVersion, final int blockStateIndex, final long[] blockStates) {
		int bits = blockStates.length >> 6;

		if (dataVersion < 2527) {
			double blockStatesIndex = blockStateIndex / (4096D / blockStates.length);
			int longIndex = (int) blockStatesIndex;
			int startBit = (int) ((blockStatesIndex - Math.floor(blockStatesIndex)) * 64D);
			if (startBit + bits > 64) {
				long prev = bitRange(blockStates[longIndex], startBit, 64);
				long next = bitRange(blockStates[longIndex + 1], 0, startBit + bits - 64);
				return (int) ((next << 64 - startBit) + prev);
			} else {
				return (int) bitRange(blockStates[longIndex], startBit, startBit + bits);
			}
		} else {
			int indicesPerLong = (int) (64D / bits);
			int blockStatesIndex = blockStateIndex / indicesPerLong;
			int startBit = (blockStateIndex % indicesPerLong) * bits;
			return (int) bitRange(blockStates[blockStatesIndex], startBit, startBit + bits);
		}
	}
	
	public static void setPaletteIndex(final int dataVersion, int blockIndex, int paletteIndex, long[] blockStates) {
		int bits = blockStates.length / 64;
		if (dataVersion < 2527) {
			double blockStatesIndex = blockIndex / (4096D / blockStates.length);
			int longIndex = (int) blockStatesIndex;
			int startBit = (int) ((blockStatesIndex - Math.floor(longIndex)) * 64D);
			if (startBit + bits > 64) {
				blockStates[longIndex] = updateBits(blockStates[longIndex], paletteIndex, startBit, 64);
				blockStates[longIndex + 1] = updateBits(blockStates[longIndex + 1], paletteIndex, startBit - 64, startBit + bits - 64);
			} else {
				blockStates[longIndex] = updateBits(blockStates[longIndex], paletteIndex, startBit, startBit + bits);
			}
		} else {
			int indicesPerLong = (int) (64D / bits);
			int blockStatesIndex = blockIndex / indicesPerLong;
			int startBit = (blockIndex % indicesPerLong) * bits;
			blockStates[blockStatesIndex] = updateBits(paletteIndex, blockStates[blockStatesIndex], startBit, startBit + bits);
		}
	}
	
	public static int getBlockstateIndex(int x, int y, int z) {
		return y * 256 + z * 16 + x;
	}

	public static int getBiomeIndex(int x, int y, int z) {
		return y * 16 + z * 4 + x;
	}

	public static int getMCRIndex(int x, int y, int z, int yBase) {
        return (x << 11) | (z << 7) | (y + (yBase << 4));
	}

	public static long bitRange(long value, int from, int to) {
		int waste = 64 - to;
		return (value << waste) >>> (waste + from);
	}

	public static byte[] reorderByteArray(byte[] arr) {
		ByteBuffer buff = ByteBuffer.allocate(BLOCK_ARR_SIZE);
		for (int x = 0; x < 16; x++) {
			for (int z = x; z < x + 256; z += 16) {
				for (int y = z; y < z + BLOCK_ARR_SIZE; y += 256) {
					buff.put(arr[y]);
				}
			}
		}
		return buff.array();
	}

	public static byte[] reorderNibbleArray(byte[] arr) {
		byte[] result = new byte[NIBBLE_ARR_SIZE];
		int i = 0;
		for (int x = 0; x < 8; x++) {
			for (int z = 0; z < 16; z++) {
				int zx = z << 3 | x;
				for (int y = 0; y < 8; y++) {
					int j = y << 8 | zx;
					int j80 = j | 0x80;
					if (arr[j] != 0 || arr[j80] != 0) {
						byte i1 = arr[j];
						byte i2 = arr[j80];
						result[i] = (byte) (i2 << 4 | i1 & 0xF);
						result[i | 0x80] = (byte) ((i1 & 0xFF) >>> 4 | i2 & 0xF0);
					}
					i++;
				}
			}
			i += 128;
		}
		return result;
	}
	
	public static long updateBits(long n, long m, int i, int j) {
		long mShifted = i > 0 ? (m & ((1L << j - i) - 1)) << i : (m & ((1L << j - i) - 1)) >>> -i;
		return ((n & ((j > 63 ? 0 : (~0L << j)) | (i < 0 ? 0 : ((1L << i) - 1L)))) | mShifted);
	}
	
	public static byte nibble4(final byte[] data, final int index) {
		byte value = data[index / 2];
		return (index % 2 == 0) ? (byte) (value & 0x0f) : (byte) ((value & 0xf0) >> 4);
	}

	public static void putNibble4(final byte[] data, final int index, final byte value) {
		int shift = (index % 2) << 2;
		data[index / 2] = (byte) ((data[index / 2] & (0xF0 >> shift)) | ((value & 0xF) << shift));
	}
	
	public static byte metabit(byte meta) {
		return (byte) ((meta & 0x3) ^ 0x1);
	}
	
	public static WorldFormat guessWorldFormat(String worldname) {
		String worldDir = Bukkit.getWorldContainer() + File.separator + worldname;
		String[] fileList = null;
		String[] possibleFolders = new String[] { "region", "region_old", "db" };
		
		for (String folder : possibleFolders) {
			File regionDir = new File(worldDir, folder);
			if (regionDir.isDirectory() && (fileList = regionDir.list()).length > 0) {
				break;
			}
		}
		
		if (fileList == null || fileList.length == 0) {
			return WorldFormat.UNKNOWN;
		}
		
		String region = fileList[0];
		if (region.endsWith(".mca")) {
			return WorldFormat.ANVIL;
		} else if (region.endsWith(".mcapm")) {
			return WorldFormat.PMANVIL;
		} else if (region.endsWith(".mcr")) {
			return WorldFormat.MCREGION;
		} else {
			return WorldFormat.LEVELDB;
		}
	}
	
	public enum WorldFormat {
		ANVIL("Anvil (1.16)", ".mca", 511, _1_16_DATAVERSION),
		_1_15("Anvil (1.15)", ".mca", 511, _1_15_DATAVERSION),
		_1_12("Anvil (1.12)", ".mca", 255, 1343),
		_1_8("Anvil (1.8)", ".mca", 192, 0),
		PMANVIL("PocketMine-Anvil (Bedrock 1.16)", ".mcapm", 255, 0),
		MCREGION("MCRegion", ".mcr", 255, 0),
		LEVELDB("LevelDB (Bedrock)", "", 511, 0),
		NUKKIT("Nukkit-Anvil (Bedrock)", ".mca", 255, 0),
		POWERNUKKIT("Nukkit-Anvil (Bedrock 1.16)", ".mca", 511, 0),
		UNKNOWN("Unknown", "", 0, 0);
		
		public String name, ext;
		public int blockIdLimit;
		public int dataVersion;
		
		WorldFormat(String name, String ext, int blockIdLimit, int dataVersion) {
			this.name = name;
			this.ext = ext;
			this.blockIdLimit = blockIdLimit;
			this.dataVersion = dataVersion;
		}
	}
}
