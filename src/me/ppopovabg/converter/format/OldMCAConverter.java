package me.ppopovabg.converter.format;

import static me.ppopovabg.converter.format.RegionFormat.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringJoiner;
import java.util.WeakHashMap;
import java.util.stream.Collectors;

import org.geysermc.connector.common.ChatColor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mojang.nbt.ByteTag;
import com.mojang.nbt.CompoundTag;
import com.mojang.nbt.EndTag;
import com.mojang.nbt.FloatTag;
import com.mojang.nbt.IntTag;
import com.mojang.nbt.ListTag;
import com.mojang.nbt.Tag;

import me.ppopovabg.converter.format.MCAUtil.BaseBlock;
import me.ppopovabg.converter.format.RegionFormat.WorldFormat;

/**
 * Converts MC Java 1.13 - 1.16 Anvil to Old Anvil
 */
public class OldMCAConverter extends RegionConverter {
	
	private static Map<Integer, Integer> BLOCK_MAP = new HashMap();
	private static Map<String, Integer> _1_12_BLOCK_MAP = new HashMap();
	private static Map<String, Integer> ITEM_MAP = new HashMap();
	static final Map<String, String> PAINTING_MOTIFS = new HashMap();
	
	public OldMCAConverter(String worldname, WorldFormat outputPlatform) {
		super(worldname, "region_old", "region", ".mca", outputPlatform);
	}
	
	@Override
	protected CompoundTag convertChunkNBT(CompoundTag oldChunk, int dataVersion) {
		CompoundTag root = new CompoundTag();
		root.putInt("DataVersion", outputFormat.dataVersion);

		int chunkGlobalX = oldChunk.getInt("xPos");
		int chunkGlobalZ = oldChunk.getInt("zPos");
		CompoundTag level = emptyChunkNBT(chunkGlobalX, chunkGlobalZ);

		ListTag oldSections = oldChunk.getList("Sections");
		ListTag<CompoundTag> sections = new ListTag(), entities = new ListTag();
		ListTag<CompoundTag> tiles = mapTileEntities(oldChunk);
		Map<Integer, CompoundTag> oldEntities = mapEntities(oldChunk, entities);

		int affectedBlocksCount = 0;
		for (int si = 0; si < oldSections.size(); ++si) {
			CompoundTag oldSection = (CompoundTag) oldSections.get(si);
			ListTag palette = oldSection.getList("Palette");
			if (palette.size() == 0) continue;

			byte[] blocks = new byte[BLOCK_ARR_SIZE];
			byte[] blockData = new byte[NIBBLE_ARR_SIZE];
			long[] blockStates = oldSection.getLongArray("BlockStates");
			int sectionY = oldSection.getByte("Y");
			
			for (int y = 0; y < 16; y++) {
				for (int z = 0; z < 16; z++) {
					for (int x = 0; x < 16; x++) {
						int blockIndex = getBlockstateIndex(x, y, z);
						int paletteIndex = getPaletteIndex(dataVersion, blockIndex, blockStates);
						CompoundTag block = (CompoundTag) palette.get(paletteIndex);

						if (block == null) {
							continue;
						}
						
						String namespace = block.getString("Name");
						int id = 0, meta = 0;
						if (namespace.equals("minecraft:air") == false) {
							CompoundTag propertiesTag = block.getCompound("Properties");
							BaseBlock newBlock = translateBlock(namespace, propertiesTag);
							id = newBlock.id;
							meta = newBlock.meta;
							
							if (id == 0) {
								unknownBlocks.add(namespace.replace("minecraft:", ""));
								continue;
							}
							
							if (id > outputFormat.blockIdLimit) {
								unknownBlocks.add(ChatColor.RED + "^" + ChatColor.GRAY + namespace.replace("minecraft:", ""));
								continue;
							}
							
							if (MCAUtil.TILE_BLOCKS.contains(id)) {
								CompoundTag blockEntity = convertBlockEntity(namespace, propertiesTag);
								if (blockEntity != null) {
									tiles.add(blockEntity);
								}
							}
						} else {
							CompoundTag oldFrameTag = (CompoundTag) oldEntities.get(getMCRIndex(x, y, z, sectionY));
							if (oldFrameTag == null) {
								continue;
							}
							
							int realX = chunkGlobalX * 16 + x, realY = sectionY * 16 + y, realZ = chunkGlobalZ * 16 + z;
							id = 199;
							meta = metabit(oldFrameTag.getByte("Facing"));
							tiles.add(translateItemFrame(realX, realY, realZ, oldFrameTag));
						}
						
						blocks[blockIndex] = (byte) id;
						putNibble4(blockData, blockIndex, (byte) meta);
						affectedBlocksCount++;
					}
				}
			}

			if (affectedBlocksCount > 0) {
				CompoundTag section = new CompoundTag();
				section.putByte("Y", (byte) sectionY);
				section.putByteArray("Data", blockData);
				section.putByteArray("Blocks", blocks);
				section.putByteArray("BlockLight", new byte[2048]);
				section.putByteArray("SkyLight", new byte[2048]);
				sections.add(section);
			}
		}
		
		if (affectedBlocksCount == 0) {
			return null;
		}
		
		level.put("Sections", sections);
		
		level.put("TileEntities", tiles.size() > 0 ? tiles : new ListTag<EndTag>());
		level.put("Entities", entities.size() > 0 ? entities : new ListTag<EndTag>());
		level.putByteArray("Biomes", convertBiomesArray(oldChunk));
		
		root.putCompound("Level", level);

		blocksConvertedInRegionCount += affectedBlocksCount;
		return root;
	}
	
	private static final BaseBlock BASE_BLOCK_AIR = new BaseBlock(0, 0);

	private BaseBlock translateBlock(String namespace, CompoundTag properties) {
		int hashcode = namespace.hashCode();
		boolean propertyCheck = false;
		
		for (Tag tag : properties.getAllTags()) {
			String prop = "";
			if (tag instanceof ByteTag) {
				byte value = properties.getByte(tag.getName());
				if (value == 1) {
					prop = tag.getName() + "-true";
					propertyCheck = true;
				} else {
					prop = tag.getName() + "-false";
				}
			} else if (tag instanceof IntTag) {
				prop = tag.getName() + "-" + properties.getInt(tag.getName());
			} else {
				String val = properties.getString(tag.getName());
				if (namespace.indexOf("wall") > -1) {
					if (val.equals("tall") || val.equals("low")) {
						val = "true";
					} else if (val.equals("none")) {
						val = "false";
					}
				}
				prop = tag.getName() + "-" + val;
			}
			hashcode ^= prop.hashCode();
		}
		
		int legacyBlock = BLOCK_MAP.getOrDefault(hashcode, 0);
		if (legacyBlock == 0) {
			legacyBlock = BLOCK_MAP.getOrDefault(namespace.hashCode(), 0);
			if (legacyBlock == 0 || propertyCheck) {
				return BASE_BLOCK_AIR;
			}
		}
		
		return new BaseBlock(legacyBlock >> 4, legacyBlock & 0xF);
	}

	private static CompoundTag emptyChunkNBT(int x, int z) {
		CompoundTag level = new CompoundTag();
		level.putInt("xPos", x);
		level.putInt("zPos", z);
		level.putLong("LastUpdate", 0);
		level.putByte("LightPopulated", (byte) 0);
		level.putLong("InhabitedTime", 0);
		level.putByte("TerrainPopulated", (byte) 1);
		level.putIntArray("HeightMap", new int[1024]);

		return level;
	}

	@Override
	protected CompoundTag convertLevelNBT(CompoundTag oldLevel) {
		CompoundTag level = new CompoundTag();
		CompoundTag data = new CompoundTag();
		data.putByte("allowCommands", (byte) 1);
		data.putDouble("BorderCenterX", 0D);
		data.putDouble("BorderCenterZ", 0D);
		data.putDouble("BorderDamagePerBlock", 0.2D);
		data.putDouble("BorderSize", 60000000D);
		data.putDouble("BorderSafeZone", 5D);
		data.putDouble("BorderSizeLerpTarget", 60000000D);
		data.putLong("BorderSizeLerpTime", 0L);
		data.putDouble("BorderWarningBlocks", 5D);
		data.putDouble("BorderWarningTime", 15D);
		data.putInt("DataVersion", outputFormat.dataVersion);
		data.putLong("DayTime", 0L);
		data.putByte("Difficulty", (byte) 0);
		data.putCompound("GameRules", new CompoundTag());
		data.putLong("RandomSeed", 0L);
		data.putString("generatorName", "flat");
		CompoundTag worldgen = oldLevel.getCompound("WorldGenSettings")
				.getCompound("dimensions")
				.getCompound("minecraft:overworld")
				.getCompound("generator");
		if (worldgen.getString("type").equals("minecraft:flat")
		&& ((CompoundTag) worldgen.getCompound("settings").getList("layers").get(0)).getString("block").equals("minecraft:bedrock")) {
			// Regular flat world
			data.putString("generatorOptions", new String());
		} else {
			// Void world
			data.putString("generatorOptions", "1;0;1");
		}
		data.putInt("GameType", 1);
		data.putByte("hardcore", (byte) 0);
		data.putLong("LastPlayed", oldLevel.getLong("LastPlayed"));
		data.putString("LevelName", oldLevel.getString("LevelName"));
		data.putByte("raining", (byte) 0);
		data.putByte("thundering", (byte) 0);
		data.putInt("rainTime", 1111);
		data.putInt("thunderTime", 1111);
		data.putLong("Time", oldLevel.getLong("Time"));
		data.putInt("SpawnX", oldLevel.getInt("SpawnX"));
		data.putInt("SpawnY", oldLevel.getInt("SpawnY"));
		data.putInt("SpawnZ", oldLevel.getInt("SpawnZ"));
		data.putInt("version", 19133);
		level.put("Data", data);

		return level;
	}
	
	private static byte[] convertBiomesArray(final CompoundTag chunk) {
		byte[] biomes = new byte[256];
		int[] oldBiomes = chunk.getIntArray("Biomes");
		if (oldBiomes != null && oldBiomes.length == _1_16_BIOME_SIZE) {
			for (int x = 0; x < 16; x++) {
				for (int z = 0; z < 16; z++) {
					int biomeId = oldBiomes[getBiomeIndex(x >> 4, 16, z >> 4)] & 0xFF;
					biomes[z * 16 + x] = (byte) biomeId;
				}
			}
		}
		return biomes;
	}
	
	private ListTag<CompoundTag> mapTileEntities(final CompoundTag chunk) {
		ListTag<? extends Tag> oldTilesTag = chunk.getList("TileEntities");
		ListTag<CompoundTag> tiles = new ListTag<>();
		
		for (int i = 0; i < oldTilesTag.size(); i++) {
			CompoundTag tile = (CompoundTag) oldTilesTag.get(i);
			String tileId = tile.getString("id").replace("minecraft:", "");
			if (tileId.equals("sign")) {
				int x = tile.getInt("x");
				int y = tile.getInt("y");
				int z = tile.getInt("z");
				tile = convertSign(tile);
				tile.putInt("x", x);
				tile.putInt("y", y);
				tile.putInt("z", z);
			} else {
				if (tileId.contains("_")) {
					String part2 = tileId.substring(tileId.indexOf("_") + 1, tileId.length());
					tileId = tileId.substring(0, 1).toUpperCase() + tileId.substring(1, tileId.indexOf("_")) + part2.substring(0, 1).toUpperCase() + part2.substring(1);
				} else {
					tileId = tileId.substring(0, 1).toUpperCase() + tileId.substring(1);
				}
				tile.putString("id", tileId);
			}
			tiles.add(tile);
		}
		
		return tiles;
	}
	
	private CompoundTag convertSign(final CompoundTag oldTile) {
		CompoundTag sign = new CompoundTag();
		if (oldTile != null) {
			if (outputFormat == WorldFormat._1_12) {
				sign = oldTile;
			} else {
				StringJoiner str = new StringJoiner("\n");
				for (int k = 1; k <= 4; k++) {
					String text = oldTile.getString("Text" + k);
					if (text.indexOf("{\"text\":") == 0) {
						// Trim the '{"text":"' and '"}'
						str.add(text.substring(9, text.length() - 2));
					} else if (text.indexOf("\"") == 0) {
						// Trim enclosing quotes
						str.add(text.substring(1, text.length() - 1));
					} else {
						// Blank line
						str.add(text);
					}
				}
				sign.putString("Text", str.toString());
			}
		}
		sign.putString("id", "Sign");
		return sign;
	}
	
	private CompoundTag translateItemFrame(int x, int y, int z, final CompoundTag oldFrameTag) {
		CompoundTag frame = new CompoundTag();
		frame.putString("id", "ItemFrame");
		frame.putInt("x", x);
		frame.putInt("y", y);
		frame.putInt("z", z);
		frame.putFloat("ItemDropChance", (float) 1.0);
		
		CompoundTag itemTag = oldFrameTag.getCompound("Item");
		String itemName = itemTag.getString("id");
		short itemId = 0, itemMeta = 0;
		if (itemName.length() > 0) {
			int itemCombined = getItem(itemName);
			if (itemCombined != 0) {
				itemId = (short) (itemCombined >> 4);
				itemMeta = (short) (itemCombined & 0xF);
			} else {
				log(ChatColor.GRAY + "Unknown item in item frame: " + itemName);
			}
		}
		
		CompoundTag item = new CompoundTag();
		
		item.putShort("id", itemId);
		item.putShort("Damage", itemMeta);
		item.putByte("Count", (byte) 1);
		frame.putCompound("Item", item);
		frame.putByte("ItemRotation", oldFrameTag.getByte("ItemRotation"));
		
		return frame;
	}
	
	static CompoundTag convertBlockEntity(String namespace, CompoundTag propertiesTag) {
		CompoundTag tile = new CompoundTag();
		if (namespace.contains("_banner")) {
			tile.putString("id", "Banner");
			int base = 0;
			int end = namespace.indexOf("wall") == -1 ? 7 : 12;
			String colorString = namespace.substring(10, namespace.length() - end);
			switch (colorString) {
			case "red": 	base = 1; break;
			case "green": 	base = 2; break;
			case "brown": 	base = 3; break;
			case "blue": 	base = 4; break;
			case "purple": 	base = 5; break;
			case "cyan": 	base = 6; break;
			case "light_gray":base = 7; break;
			case "gray": 	base = 8; break;
			case "pink": 	base = 9; break;
			case "lime": 	base = 10; break;
			case "yellow": 	base = 11; break;
			case "light_blue":base = 12; break;
			case "magenta": base = 13; break;
			case "orange": 	base = 14; break;
			case "white": 	base = 15; break;
			}
			tile.putInt("Base", base);
		} else if (namespace.contains("_skull")) {
			tile.putString("id", "Skull");
			byte skullType = 0;
			if (namespace.indexOf("wither") != -1) 		skullType = 1;
			else if (namespace.indexOf("zombie") != -1) skullType = 2;
			else if (namespace.indexOf("player") != -1) skullType = 3;
			else if (namespace.indexOf("creeper") != -1)skullType = 4;
			else if (namespace.indexOf("dragon") != -1) skullType = 5;
			if (namespace.indexOf("wall") == -1) {
				String rot = propertiesTag.getString("rotation");
				if (!rot.isEmpty()) {
					tile.putByte("Rot", Byte.valueOf(rot));
				}
			}
			tile.putByte("SkullType", skullType);
		} else if (namespace.contains("potted_")) {
			tile.putString("id", "FlowerPot");
			int flower = BLOCK_MAP.get(namespace.hashCode());
			tile.putShort("item", (short) (flower >> 4));
			tile.putShort("Item", (short) (flower >> 4));
			tile.putInt("data", flower & 0xF);
			tile.putInt("Data", flower & 0xF);
			tile.putInt("mData", flower & 0xF); // PocketMine
		} else if (namespace.contains("_bed")) {
			tile.putString("id", "Bed");
			byte color = 0;
			switch (namespace) {
			case "minecraft:orange_bed": 	color = 1; break;
			case "minecraft:magenta_bed": 	color = 2; break;
			case "minecraft:light_blue_bed":color = 3; break;
			case "minecraft:yellow_bed": 	color = 4; break;
			case "minecraft:lime_bed": 		color = 5; break;
			case "minecraft:pink_bed": 		color = 6; break;
			case "minecraft:gray_bed": 		color = 7; break;
			case "minecraft:light_gray_bed":color = 8; break;
			case "minecraft:cyan_bed": 		color = 9; break;
			case "minecraft:purple_bed": 	color = 10; break;
			case "minecraft:blue_bed": 		color = 11; break;
			case "minecraft:brown_bed": 	color = 12; break;
			case "minecraft:green_bed": 	color = 13; break;
			case "minecraft:red_bed": 		color = 14; break;
			case "minecraft:black_bed": 	color = 15; break;
			}
			tile.putByte("color", color);
		} else {
			return null;
		}
		return tile;
	}
	
	static CompoundTag convertTile(int id, String namespace, CompoundTag propertiesTag) {
		CompoundTag tile = new CompoundTag();
		
		switch (id) {
		case 176:
		case 177:
			tile.putString("id", "Banner");
			int base = 0;
			int end = namespace.indexOf("wall") == -1 ? 7 : 12;
			String colorString = namespace.substring(10, namespace.length() - end);
			switch (colorString) {
			case "red": 	base = 1; break;
			case "green": 	base = 2; break;
			case "brown": 	base = 3; break;
			case "blue": 	base = 4; break;
			case "purple": 	base = 5; break;
			case "cyan": 	base = 6; break;
			case "light_gray":base = 7; break;
			case "gray": 	base = 8; break;
			case "pink": 	base = 9; break;
			case "lime": 	base = 10; break;
			case "yellow": 	base = 11; break;
			case "light_blue":base = 12; break;
			case "magenta": base = 13; break;
			case "orange": 	base = 14; break;
			case "white": 	base = 15; break;
			}
			tile.putInt("Base", base);
			break;
		case 144:
			tile.putString("id", "Skull");
			byte skullType = 0;
			if (namespace.indexOf("wither") != -1) 		skullType = 1;
			else if (namespace.indexOf("zombie") != -1) skullType = 2;
			else if (namespace.indexOf("player") != -1) skullType = 3;
			else if (namespace.indexOf("creeper") != -1)skullType = 4;
			else if (namespace.indexOf("dragon") != -1) skullType = 5;
			if (namespace.indexOf("wall") == -1) {
				String rot = propertiesTag.getString("rotation");
				if (!rot.isEmpty()) {
					tile.putByte("Rot", Byte.valueOf(rot));
				}
			}
			tile.putByte("SkullType", skullType);
			break;
		case 140:
			tile.putString("id", "FlowerPot");
			int flower = BLOCK_MAP.get(namespace.hashCode());
			tile.putShort("item", (short) (flower >> 4));
			tile.putShort("Item", (short) (flower >> 4));
			tile.putInt("data", flower & 0xF);
			tile.putInt("Data", flower & 0xF);
			tile.putInt("mData", flower & 0xF); // PocketMine
			//tile.print(System.out);
			break;
		case 26:
			tile.putString("id", "Bed");
			byte color = 0;
			switch (namespace) {
			case "minecraft:orange_bed": 	color = 1; break;
			case "minecraft:magenta_bed": 	color = 2; break;
			case "minecraft:light_blue_bed":color = 3; break;
			case "minecraft:yellow_bed": 	color = 4; break;
			case "minecraft:lime_bed": 		color = 5; break;
			case "minecraft:pink_bed": 		color = 6; break;
			case "minecraft:gray_bed": 		color = 7; break;
			case "minecraft:light_gray_bed":color = 8; break;
			case "minecraft:cyan_bed": 		color = 9; break;
			case "minecraft:purple_bed": 	color = 10; break;
			case "minecraft:blue_bed": 		color = 11; break;
			case "minecraft:brown_bed": 	color = 12; break;
			case "minecraft:green_bed": 	color = 13; break;
			case "minecraft:red_bed": 		color = 14; break;
			case "minecraft:black_bed": 	color = 15; break;
			}
			tile.putByte("color", color);
			break;
		case 54:
			tile.putString("id", "Chest");
			tile.put("Items", new ListTag<CompoundTag>());
			break;
		case 61:
			tile.putString("id", "Furnace");
			tile.put("Items", new ListTag<CompoundTag>());
			tile.putShort("BurnTime", (short)0);
			tile.putShort("CookTime", (short)0);
			tile.putShort("CookTimeTotal", (short)0);
			tile.putCompound("RecipesUsed", new CompoundTag());
			break;
		case 146:
			tile.putString("id", "TrappedChest");
			tile.put("Items", new ListTag<CompoundTag>());
			break;
		case 116: tile.putString("id", "EnchTable"); break;
		case 125:
			tile.putString("id", "Dropper");
			tile.put("Items", new ListTag<CompoundTag>());
			break;
		case 130:
			tile.putString("id", "EnderChest");
			tile.put("Items", new ListTag<CompoundTag>());
			break;
		case 138:
			tile.putString("id", "Beacon");
			tile.putInt("Levels", 0);
			tile.putInt("Primary", 0);
			tile.putInt("Secondary", 0);
			break;
		case 149:
		case 150:
			tile.putString("id", "Comparator");
			tile.putInt("OutputSignal", 0);
			break;
		case 151: tile.putString("id", "DaylightDetector"); break;
		case 154:
			tile.putString("id", "Hopper");
			tile.put("Items", new ListTag<CompoundTag>());
			tile.putInt("TransferCooldown", 0);
			break;
		case 117:
			tile.putString("id", "BrewingStand");
			tile.put("Items", new ListTag<CompoundTag>());
			tile.putShort("BrewTime", (short)0);
			tile.putByte("Fuel", (byte)0);
			break;
		case 23:
			tile.putString("id", "Dispenser");
			tile.put("Items", new ListTag<CompoundTag>());
			break;
		case 25:
			tile.putString("id", "NoteBlock");
			break;
		}
		return tile;
	}

	private static Map<Integer, CompoundTag> mapEntities(CompoundTag chunk, ListTag<CompoundTag> entities) {
		ListTag oldEntitiesTag = chunk.getList("Entities");
		Map<Integer, CompoundTag> oldEntities = new WeakHashMap<>();
	
		for (int ti = 0; ti < oldEntitiesTag.size(); ++ti) {
			CompoundTag entity = (CompoundTag) oldEntitiesTag.get(ti);
			switch (entity.getString("id")) {
			case "item_frame":
			case "minecraft:item_frame":
				int y = entity.getInt("TileY");
				oldEntities.put(getMCRIndex(entity.getInt("TileX") & 0xF, y & 0xF, entity.getInt("TileZ") & 0xF, y >> 4), entity);
				break;
			case "painting":
			case "minecraft:painting":
				entity.putString("id", "Painting");
				String motif = entity.getString("Motive");
				String newMotif = PAINTING_MOTIFS.get(motif);
				if (newMotif != null) {
					entity.putString("Motive", newMotif);
				} else {
					motif = motif.substring(motif.indexOf(":") + 1);
					// Capitalize first letter
					entity.putString("Motive", motif.substring(0, 1).toUpperCase() + motif.substring(1));
				}
				
				byte facing = entity.getByte("Facing");
				float pitch = 0;
				switch (facing) {
				case 0: pitch = 270; break;
				case 1: pitch = 360; break;	
				case 2: pitch = 180; break;
				case 3: pitch = 90; break;
				}
				entity.putByte("Direction", facing);
				
				ListTag rot = new ListTag();
				rot.add(new FloatTag("", pitch));
				rot.add(new FloatTag("", 0));
				entity.put("Rotation", rot);
				
				entities.add(entity);
			}
		}
		
		return oldEntities;
	}

	static int getItem(String namespace) {
		int combined = ITEM_MAP.getOrDefault(namespace, 0);
		if (combined == 0) {
			combined = BLOCK_MAP.getOrDefault(namespace, 0);
		}
		return combined;
	}
	
	@Override
	protected boolean verifyDataVersion(int dataVersion) {
		boolean result = dataVersion > WorldFormat._1_12.dataVersion;
		if (result == false) {
			log("Chunk is not in new-Anvil format; skipping region.");
		}
		return result;
	}
	
	public static void loadBlockTable(InputStream stream) throws IOException {
		Properties blockMap = new Properties();
		blockMap.load(stream);
		
		BLOCK_MAP = Collections.unmodifiableMap(
			blockMap.entrySet()
				.stream()
				.collect(Collectors.toMap(
					entry -> {
						String key = "minecraft:" + String.valueOf(entry.getKey()).toLowerCase();
						int hashcode = 0;
						for (String propStr : key.split(";")) {
							hashcode ^= propStr.hashCode();
						}
						return hashcode;
					},
					entry -> {
						String[] split = entry.getValue().toString().split(",");
						return (Integer.valueOf(split[0]) << 4) | Integer.valueOf(split[1]);
					},
					(e1, e2) -> e1)
				));
	}
	
	public static void loadOldBlockTable(JSONParser parser, InputStream stream) throws UnsupportedEncodingException, IOException, ParseException {
		final JSONObject root = (JSONObject) parser.parse(new InputStreamReader(stream, "UTF-8"));
		root.keySet().forEach(key -> {
			String[] data = root.get(key).toString().split(":");
			int id = Integer.valueOf(data[0]);
			int meta = data.length > 1 ? Integer.valueOf(data[1]) : 0;
			_1_12_BLOCK_MAP.put(key.toString(), (id << 4) | meta);
		});
		parser.reset();
	}
	
	public static void loadItemTable(JSONParser parser, InputStream stream) throws UnsupportedEncodingException, IOException, ParseException {
		final JSONObject root = (JSONObject) parser.parse(new InputStreamReader(stream, "UTF-8"));
		root.keySet().forEach(key -> {
			String[] data = root.get(key).toString().split(":");
			int id = Integer.valueOf(data[0]);
			int meta = data.length > 1 ? Integer.valueOf(data[1]) : 0;
			ITEM_MAP.put("minecraft:" + key.toString(), (id << 4) | meta);
		});
		parser.reset();
	}
	
	static class LegacyMatadata {
		public int id;
		public int meta;
		public Map<Integer, Integer> metadata = new HashMap<>();

		public LegacyMatadata(int id, int meta, Map metadata) {
			this.id = id;
			this.meta = meta;
			this.metadata = metadata;
		}
	}
}
