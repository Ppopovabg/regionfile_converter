package me.ppopovabg.converter.format;

import static me.ppopovabg.converter.format.RegionFormat.*;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.stream.Stream;

import javax.annotation.Nullable;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.bukkit.block.BlockFace;

import com.mojang.nbt.*;

import me.ppopovabg.converter.format.MCAConverter.LegacyMaterial.MaterialType;
import me.ppopovabg.converter.format.MCAUtil.BaseBlock;
import me.ppopovabg.converter.format.MCAUtil.BlockPosition;

/**
 * Converts Bedrock Anvil, PMAnvil and MCRegion to MC Java 1.15 Anvil
 */
public class MCAConverter extends RegionConverter {

	private static final LegacyMaterial[] BLOCK_MAP = new LegacyMaterial[512];
	private static final Map<Integer, String> ITEM_MAP = new HashMap<>();
	private static final int ARG_KEEP_BIOMES = 0x1;

	private final OldDataLayer dataLayer;
	private final int args;
	
	public MCAConverter(Class<? extends OldDataLayer> dataLayerClass,
						String worldname,
						WorldFormat inputFormat) {
		super(worldname, "region_old", "region", inputFormat.ext, WorldFormat.ANVIL);

		OldDataLayer layerInstance = null;
		try {
			layerInstance = dataLayerClass.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			stop();
		}
		this.dataLayer = layerInstance;
		this.args = ARG_KEEP_BIOMES;
	}

	@Override
	protected CompoundTag convertChunkNBT(CompoundTag oldChunk, int dataVersion) {
		CompoundTag root = new CompoundTag();
		root.putInt("DataVersion", outputFormat.dataVersion);

		int chunkGlobalX = oldChunk.getInt("xPos");
		int chunkGlobalZ = oldChunk.getInt("zPos");
		CompoundTag level = emptyChunkNBT(chunkGlobalX, chunkGlobalZ);

		ListTag<? extends Tag> oldSections = oldChunk.getList("Sections");
		int sectionsSize = oldSections.size();
		boolean isFixedSectionSize = dataLayer.hasFixedSectionSize();
		byte[] blocks = null, blockData = null, blockLight = null, skyLight = null;
		
		if (isFixedSectionSize) {
			blocks = dataLayer.extractBlocks(oldChunk);
			if (blocks.length == 0) {
				return null;
			}
			blockData = dataLayer.extractBlockData(oldChunk);
			blockLight = dataLayer.extractBlockLight(null);
			skyLight = dataLayer.extractSkyLight(null);
			sectionsSize = 8;
		}
		
		ListTag<CompoundTag> sections = new ListTag<>();
		ListTag<CompoundTag> entities = mapEntities(oldChunk), tiles = new ListTag<>();
		Map<Integer, CompoundTag> oldTiles = mapTileEntities(oldChunk);
		Set<Integer> blockUpdateQueue = new HashSet<>();
		
		int affectedBlocksCount = 0;
		for (int yBase = 0; yBase < sectionsSize; yBase++) {
			CompoundTag oldSection = null;
			int sectionY;
			
			if (isFixedSectionSize == false) {
				oldSection = (CompoundTag) oldSections.get(yBase);
				blocks = dataLayer.extractBlocks(oldSection);
				if (blocks.length != BLOCK_ARR_SIZE) {
					continue;
				}
				blockData = dataLayer.extractBlockData(oldSection);
				blockLight = dataLayer.extractBlockLight(oldSection);
				skyLight = dataLayer.extractSkyLight(oldSection);
				sectionY = oldSection.getByte("Y");
			} else {
				sectionY = yBase;
			}
			
			List<PalettedBlock> sectionBlocks = new ArrayList<>();
			for (int y = 15; y >= 0; y--) {
				for (int z = 0; z < 16; z++) {
					for (int x = 0; x < 16; x++) {
						int index = dataLayer.getIndex(x, y, z, yBase);
						int id = blocks[index] & 0xFF;
						if (id == 0) continue;
						if (id < 0) id += 128;
						int meta = nibble4(blockData, index);

						LegacyMaterial material = getMaterial(id);
						if (material.id == 0) {
							unknownBlocks.add(id + ":" + meta);
							continue;
						}

						PalettedBlock block = new PalettedBlock(material, meta, x, y, z);
						translateBlock(block);

						final BlockPosition realPos = new BlockPosition(chunkGlobalX * 16 + x, sectionY * 16 + y, chunkGlobalZ * 16 + z);
						if (material.hasTile) {
							CompoundTag oldTile = oldTiles.get(getMCRIndex(x, y, z, sectionY));
							CompoundTag tile = convertTile(block, realPos, oldTile);
							if (!tile.getString("id").isEmpty()) {
								tiles.add(tile);
							}
						} else if (material.is(MaterialType.STAIR)) {
							MCAUtil.calculateStairCorner(block,
									getBlockRelativeTo(realPos, BlockFace.EAST, blocks, blockData),
									getBlockRelativeTo(realPos, BlockFace.WEST, blocks, blockData),
									getBlockRelativeTo(realPos, BlockFace.SOUTH, blocks, blockData),
									getBlockRelativeTo(realPos, BlockFace.NORTH, blocks, blockData));
							
						} else if (material.canConnect) {
							if (id == 139) {
								// Wall
								BitSet connectedFaces = new BitSet(4);
								boolean connectedUp = false;
								BaseBlock above = getBlockRelativeVertical(BlockFace.UP, realPos, blocks, blockData, oldSections);
								LegacyMaterial aboveMaterial = getMaterial(above.id);
								
								block.set("up", "true");
								if (above.id > 0) {
									if (material.canConnect(aboveMaterial)) {
										connectedUp = true;
									} else if (aboveMaterial.id == 50 || aboveMaterial.id == 76) { // Torch
										if (!(above.meta > 0 && above.meta < 5)) {
											connectedUp = true;
										}
									} else if (aboveMaterial.is(MaterialType.FENCE_GATE)) {
										block.set("up", "false");
									}
								}
								
								for (int f=0; f < 4; f++) {
									BlockFace face = MCAUtil.DIRECT_BLOCK_FACES.get(f);
									BaseBlock rel = getBlockRelativeTo(realPos, face, blocks, blockData);
									LegacyMaterial relMat = getMaterial(rel.id);
									
									if (material.canConnect(relMat)
									|| (relMat.is(MaterialType.STAIR) && (rel.meta & 0x3) == MCAUtil.rotateBlockFace(f))
									|| (relMat.is(MaterialType.FENCE_GATE) && (rel.meta & 0x1) == (f > 1 ? 1 : 0))) {
										String height = "low";
										if (connectedUp) {
											if (aboveMaterial.canConnect) {
												LegacyMaterial upNeighbour = getMaterial(
														getBlockRelativeVertical(BlockFace.UP, realPos, blocks, null, oldSections).id);
												if (aboveMaterial.canConnect(upNeighbour) || upNeighbour.is(MaterialType.FENCE_GATE)) {
													height = "tall";
												}
											} else {
												height = "tall";
											}
										}
										block.set(face.toString().toLowerCase(), height);
										connectedFaces.set(f);
									}
								}
								
								if (blockUpdateQueue.remove(index)) {
									block.set("up", "true");
								} else if (((connectedFaces.get(0) && connectedFaces.get(1) && connectedFaces.get(2) && connectedFaces.get(3))
								|| ((connectedFaces.get(0) && connectedFaces.get(1) && !connectedFaces.get(2) && !connectedFaces.get(3))
									|| (connectedFaces.get(2) && connectedFaces.get(3) && !connectedFaces.get(0) && !connectedFaces.get(1))))) {
									block.set("up", "false");
								}
								
								if ("true".equals(block.get("up"))) {
									// Give its up state to the walls below it as well
									int searchY = y;
									if ((searchY-1) < 0) {
										searchY = 15;
									} else {
										--searchY;
									}
									BaseBlock below = getBlockRelativeVertical(BlockFace.DOWN, realPos, blocks, null, oldSections);
									if (below.id == id) {
										blockUpdateQueue.add(getBlockstateIndex(x, searchY, z));
									}
								}
							} else {
								for (int f=0; f < 4; ++f) {
									BlockFace face = MCAUtil.DIRECT_BLOCK_FACES.get(f);
									BaseBlock rel = getBlockRelativeTo(realPos, face, blocks, blockData);
									LegacyMaterial relMat = getMaterial(rel.id);
									
									if (material.canConnect(relMat)
									|| (relMat.is(MaterialType.STAIR) && (rel.meta & 0x3) == MCAUtil.rotateBlockFace(f))
									|| (material.is(MaterialType.FENCE)
										&& relMat.is(MaterialType.FENCE_GATE) && (rel.meta & 0x1) == (f > 1 ? 1 : 0))) {
										block.set(face.toString().toLowerCase(), "true");
									}
								}
							}
						} else if (material.is(MaterialType.DOOR) || id == 175) {
							if (meta == 8) {
								// Check whether this is the top part of a door or double plant
								// and get the data from the bottom part
								boolean hasBottom = false;
								BaseBlock below = getBlockRelativeVertical(BlockFace.DOWN, realPos, blocks, blockData, oldSections);
								if (below.id == id) {
									meta = below.meta;
									hasBottom = true;
								}
								
								if (hasBottom) {
									if (id == 175) {
										switch (meta & 0x7) {
										case 0: block.setName("sunflower"); break;
										case 1: block.setName("lilac"); break;
										case 2: block.setName("tall_grass"); break;
										case 3: block.setName("large_fern"); break;
										case 4: block.setName("rose_bush"); break;
										case 5: block.setName("peony"); break;
										}
									} else {
										switch (meta & 0x3) {
										case 0: block.set("facing", "east"); break;
										case 1: block.set("facing", "south"); break;
										case 2: block.set("facing", "west"); break;
										case 3: block.set("facing", "north"); break;
										}
										if (meta >= 4) block.set("open", "true");
									}
								}
							}
						} else if (id == 199) {
							CompoundTag oldFrameTag = oldTiles.get(getMCRIndex(x, y, z, sectionY));
							if (oldFrameTag != null) {
								entities.add(convertItemFrame(realPos, meta, oldFrameTag));
							}
						} else if (id == 140) {
							CompoundTag potTag = oldTiles.get(getMCRIndex(x, y, z, sectionY));
							if (potTag != null) {
								short item = potTag.getShort("item");
								if (item != 0) {
									int data = 0;
									for (String tagName : new String[] {"mData", "data", "Data"}) {
										if (potTag.contains(tagName)) {
											data = potTag.getInt(tagName);
											break;
										}
									}
									LegacyMetadata flower = getMaterial(item).metadata[data];
									block.setName("potted_" + flower.namespace);
								}
							}
						}
						
						sectionBlocks.add(block);
						affectedBlocksCount++;
					}
				}
			}
			
			if (!sectionBlocks.isEmpty()) {
				CompoundTag section = new CompoundTag();
				section.putByte("Y", (byte) sectionY);
				setPaletteBlocksInSection(sectionBlocks, section);
				section.putByteArray("BlockLight", blockLight);
				section.putByteArray("SkyLight", skyLight);
				sections.add(section);
			}
		}
		
		if (affectedBlocksCount == 0) {
			return null;
		}
		
		level.put("Sections", sections);
		
		level.put("TileEntities", tiles.size() > 0 ? tiles : new ListTag<EndTag>());
		level.put("Entities", entities.size() > 0 ? entities : new ListTag<EndTag>());
		level.putIntArray("Biomes", convertBiomesArray(oldChunk.getByteArray("Biomes"), oldChunk.getIntArray("BiomeColors")));
		root.putCompound("Level", level);
		
		blocksConvertedInRegionCount += affectedBlocksCount;
		return root;
	}

	private void setPaletteBlocksInSection(List<PalettedBlock> blocks, CompoundTag section) {
		String[] sectionBlocks = new String[BLOCK_ARR_SIZE];
		for (PalettedBlock block : blocks) {
			sectionBlocks[getBlockstateIndex(block.relX, block.relY, block.relZ)] = block.toString();
		}
		
		// Sort the block palette by frequency
		
		Map<String, Integer> counter = new HashMap<>();
		if (blocks.size() < BLOCK_ARR_SIZE) {
			counter.put("air", (BLOCK_ARR_SIZE - blocks.size()));
		}
		
		Stream.of(sectionBlocks)
			.filter(Objects::nonNull)
			.forEach(b -> counter.put(b, 1 + counter.getOrDefault(b, 0)));

		List<String> usedBlocks = new ArrayList<>(counter.keySet());
		usedBlocks.sort((String x, String y) -> counter.get(y) - counter.get(x));

		ListTag<CompoundTag> palette = new ListTag<>();
		for (String blockName : usedBlocks) {
			CompoundTag block = new CompoundTag();
			String[] blockArray = blockName.split(";");
			block.putString("Name", "minecraft:" + blockArray[0]);
			if (blockArray.length > 1) {
				CompoundTag properties = new CompoundTag();
				for (int i = 1; i < blockArray.length; ++i) {
					String[] property = blockArray[i].split("=");
					properties.putString(property[0], property[1]);
				}
				block.putCompound("Properties", properties);
			}
			palette.add(block);
		}
		section.put("Palette", palette);

		final int airIndex = usedBlocks.indexOf("air");

		long[] blockStates = null;
		int paletteSize = usedBlocks.size();
		if (paletteSize > 16) {
			final int paletteIndexSize = Math.max(4, (int) Math.ceil(Math.log(paletteSize) / Math.log(2)));
			blockStates = new long[64 * paletteIndexSize];
		} else {
			blockStates = new long[256];
		}
		
		for (int blockstateIndex = 0; blockstateIndex < BLOCK_ARR_SIZE; blockstateIndex++) {
			short paletteIndex = (short) (sectionBlocks[blockstateIndex] == null ? airIndex : usedBlocks.indexOf(sectionBlocks[blockstateIndex]));
			setPaletteIndex(outputFormat.dataVersion,
					blockstateIndex, paletteIndex, blockStates);
		}
		
    	section.putLongArray("BlockStates", blockStates);
	}
	
	private static final BaseBlock BASE_BLOCK_AIR = new BaseBlock(0, 0);

	private BaseBlock getBlockAt(int x, int y, int z) {
		int chunkGlobalX = x >> 4;
		int chunkGlobalZ = z >> 4;
		int regionX = chunkGlobalX >> 5;
		int regionZ = chunkGlobalZ >> 5;

		File file = new File(worldPath + "/" + inputFolder, "r." + regionX + "." + regionZ + inputFileExtension);
		if (!file.isFile()) return BASE_BLOCK_AIR;

		RegionFile region = regionFileCache.get(file);
		if (regionFileCache.size() == 16) {
			clearFileCache();
		}
		if (region == null) {
			region = new RegionFile(file);
			regionFileCache.put(file, region);
		}

		int cx = chunkGlobalX & 31;
		int cz = chunkGlobalZ & 31;
		if (region.hasChunk(cx, cz)) {
			try {
				DataInputStream regionChunkInputStream = region.getChunkDataInputStream(cx, cz);
				CompoundTag root = NbtIo.read(regionChunkInputStream);
				regionChunkInputStream.close();

				return getBlockInChunk(root.getCompound("Level"), x, y, z);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			//System.out.printf(ChatColor.GRAY + "Block at %d, %d, %d, Chunk %d, %d was not in the expected region (%d, %d)\n", x, y, z, cx, cz, regionX, regionZ);
		}
		return BASE_BLOCK_AIR;
	}
	
	private BaseBlock getBlockInChunk(CompoundTag chunk, int realX, int realY, int realZ) {
		ListTag<? extends Tag> sections = chunk.getList("Sections");
		int sectionsSize = sections.size();
		int sectionY = realY >> 4;
		byte[] blocks, blockData;
		
		if (dataLayer.hasFixedSectionSize()) {
			blocks = dataLayer.extractBlocks(chunk);
			if (blocks.length == 0) {
				return BASE_BLOCK_AIR;
			}
			blockData = dataLayer.extractBlockData(chunk);
			int index = dataLayer.getIndex(realX & 0xF, realY & 0xF, realZ & 0xF, sectionY);
			int id = blocks[index] & 0xFF;
			if (id < 0) id += 128;
			int meta = nibble4(blockData, index);
			return new BaseBlock(id, meta);
		}
		
		for (int si = 0; si < sectionsSize; si++) {
			CompoundTag oldSection = (CompoundTag) sections.get(si);
			sectionY = oldSection.getByte("Y");
			if (sectionY == (realY >> 4)) {
				blocks = dataLayer.extractBlocks(oldSection);
				if (blocks.length == 0) {
					return BASE_BLOCK_AIR;
				}
				blockData = dataLayer.extractBlockData(oldSection);
				int index = dataLayer.getIndex(realX & 0xF, realY & 0xF, realZ & 0xF, sectionY);
				int id = blocks[index] & 0xFF;
				if (id < 0) id += 128;
				int meta = nibble4(blockData, index);
				return new BaseBlock(id, meta);
			}
		}
		return BASE_BLOCK_AIR;
	}
	
	private BaseBlock getBlockRelativeTo(BlockPosition pos, BlockFace direction, final byte[] blocks, @Nullable final byte[] blockData) {
		int x = pos.x & 0xF; int y = pos.y & 0xF; int z = pos.z & 0xF;
		int x_offset = 0; int z_offset = 0; int relative = 0;
		
		switch (direction) {
		case EAST:
			relative = x+1;
			x_offset = 1;
			break;
		case WEST:
			relative = x-1;
			x_offset = -1;
			break;
		case SOUTH:
			relative = z+1;
			z_offset = 1;
			break;
		case NORTH:
			relative = z-1;
			z_offset = -1;
			break;
		default: return BASE_BLOCK_AIR;
		}
		
		if (relative >= 0 && relative < 16) {
			// Same chunk
			int index = dataLayer.getIndex(x + x_offset, y, z + z_offset, pos.y >> 4);
			int id = blocks[index] & 0xFF;
			if (id < 0) id += 128;
			if (blockData == null) return new BaseBlock(id, 0);
			int meta = nibble4(blockData, index);
			return new BaseBlock(id, meta);
		} else {
			return getBlockAt(pos.x + x_offset, pos.y, pos.z + z_offset);
		}
	}

	private BaseBlock getBlockRelativeVertical(BlockFace direction, BlockPosition realPos,
			byte[] blocks, @Nullable byte[] blockData, @Nullable final ListTag<? extends Tag> sections) {
		
		int newY = realPos.y, newYBase = realPos.y >> 4;
		boolean isInDifferentSection = false;
		if (direction == BlockFace.DOWN) {
			if (--newY < 0) {
				isInDifferentSection = true;
				--newYBase;
				newY = 15;
			}
		} else {
			if (++newY == 16) {
				isInDifferentSection = true;
				++newYBase;
				newY = 0;
			}
		}
		
		if (isInDifferentSection && sections != null) {
			if (newYBase < sections.size() && newYBase >= 0) {
				CompoundTag section = (CompoundTag) sections.get(newYBase);
				blocks = dataLayer.extractBlocks(section);
				if (blockData != null) {
					blockData = dataLayer.extractBlockData(section);
				}
				newYBase = section.getByte("Y");
			} else {
				return BASE_BLOCK_AIR;
			}
		}
		
		int index = dataLayer.getIndex(realPos.x & 0xF, newY & 0xF, realPos.z & 0xF, newYBase);
		int id = blocks[index] & 0xFF;
		if (id < 0) id += 128;
		if (blockData == null) return new BaseBlock(id, 0);
		int meta = nibble4(blockData, index);
		return new BaseBlock(id, meta);
	}

	//@formatter:off
	protected static void translateBlock(PalettedBlock block) {
		LegacyMaterial material = block.getMaterial();
		int id = material.id;
		int meta = block.getMetaValue();
		
		switch (material.type) {
		case STAIR:
			switch (meta & 0x3) {
			case 0: block.set("facing", "east"); break;
			case 1: block.set("facing", "west"); break;
			case 2: block.set("facing", "south"); break;
			case 3: block.set("facing", "north"); break;
			}
			if (meta >= 4) block.set("half", "top");
			break;
		case DOOR:
			if (meta >= 8) {
				block.set("half", "upper");
			} else {
				switch (meta & 0x3) {
				case 0: block.set("facing", "east"); break;
				case 1: block.set("facing", "south"); break;
				case 2: block.set("facing", "west"); break;
				case 3: block.set("facing", "north"); break;
				}
				if (meta >= 4) block.set("open", "true");
			}
			break;
		case FENCE_GATE:
			switch (meta & 0x3) {
			case 0: block.set("facing", "south"); break;
			case 1: block.set("facing", "west"); break;
			case 2: block.set("facing", "north"); break;
			case 3: block.set("facing", "east"); break;
			}
			if (meta >= 4) block.set("open", "true");
			if (meta >= 8) block.set("in_wall", "true");
			break;
		case GLAZED_TERRACOTTA:
			switch (meta & 0x3) {
			case 0: block.set("facing", "west"); break;
			case 1: block.set("facing", "east"); break;
			case 2: block.set("facing", "north"); break;
			case 3: block.set("facing", "south"); break;
			}
			break;
		default:
			switch (id) {
			case 17: // Log
			case 162:
				if (meta >= 12) {
					block.setName(block.getName().replace("_log", "_wood"));
				}
				break;
			case 69: // Lever
				if (block.get("face").equals("wall") && meta >= 8) {
					block.set("powered", "true");
				} else if (meta >= 8) {
					block.set("powered", "false");
				} else {
					block.set("powered", "true");
				}
				break;
			case 106: // Vines
				if ((meta & 0x1) != 0) block.set("south", "true");
				if ((meta & 0x2) != 0) block.set("west", "true");
				if ((meta & 0x4) != 0) block.set("north", "true");
				if ((meta & 0x8) != 0) block.set("east", "true");
				break;
			case 393: block.set("age", meta); break;
			}
			break;
		}
	}
	
	private static final String[] STRUCTURE_NAMES = { "buried_treasure", "desert_pyramid", "igloo", "jungle_pyramid", "mansion", "mineshaft", "monument", "ocean_ruin", "pillager_outpost", "shipwreck", "stronghold",
			"swamp_hut", "village" };

	static CompoundTag emptyChunkNBT(int x, int z) {
		CompoundTag level = new CompoundTag();
		level.putInt("xPos", x);
		level.putInt("zPos", z);
		level.putString("Status", "full");
		level.putByte("isLightOn", (byte) 1);
		level.putLong("InhabitedTime", 0);
		level.putLong("LastUpdate", 0);
		level.put("LiquidTicks", new ListTag<LongTag>());
		level.put("TileTicks", new ListTag<LongTag>());
		ListTag<ListTag<ByteTag>> postProcessing = new ListTag<>();
		for (int i = 0; i < 16; i++) postProcessing.add(new ListTag<ByteTag>());
		level.put("PostProcessing", postProcessing);
		CompoundTag structures = new CompoundTag();
		CompoundTag heightmaps = new CompoundTag();
		CompoundTag references = new CompoundTag();
		CompoundTag starts = new CompoundTag();
		for (String structure : STRUCTURE_NAMES) {
			references.putLongArray(structure, new long[0]);
			CompoundTag start = new CompoundTag();
			start.putString("id", "INVALID");
			starts.putCompound(structure, start);
		}
		structures.putCompound("References", references);
		structures.putCompound("Starts", starts);
		level.putCompound("Structures", structures);
		long[] motionBlocking = new long[37];
		long[] motionBlockingNoLeaves = new long[37];
		long[] oceanFloor = new long[37];
		long[] worldSurface = new long[37];
		for (int i = 0; i <= 36; i++) {
			motionBlocking[i] = 0xFFFFFFFFFFFFFFFFL;
			motionBlockingNoLeaves[i] = 0xFFFFFFFFFFFFFFFFL;
			oceanFloor[i] = 0xFFFFFFFFFFFFFFFFL;
			worldSurface[i] = 0xFFFFFFFFFFFFFFFFL;
		}
		heightmaps.putLongArray("MOTION_BLOCKING", motionBlocking);
		heightmaps.putLongArray("MOTION_BLOCKING_NO_LEAVES", motionBlockingNoLeaves);
		heightmaps.putLongArray("OCEAN_FLOOR", oceanFloor);
		heightmaps.putLongArray("WORLD_SURFACE", worldSurface);
		level.putCompound("Heightmaps", heightmaps);
		
		return level;
	}

	@Override
	protected CompoundTag convertLevelNBT(CompoundTag oldLevel) {
		CompoundTag level = new CompoundTag();
		CompoundTag data = new CompoundTag();
		data.putByte("allowCommands", (byte)1);
		data.putDouble("BorderCenterX", 0D);
		data.putDouble("BorderCenterZ", 0D);
		data.putDouble("BorderDamagePerBlock", 0.2D);
		data.putDouble("BorderSize", 60000000D);
		data.putDouble("BorderSafeZone", 5D);
		data.putDouble("BorderSizeLerpTarget", 60000000D);
		data.putLong("BorderSizeLerpTime", 0L);
		data.putDouble("BorderWarningBlocks", 5D);
		data.putDouble("BorderWarningTime", 15D);
		data.putInt("clearWeatherTime", 0);
		data.putCompound("CustomBossEvents", new CompoundTag());
		data.putCompound("DataPacks", new CompoundTag());
		data.putInt("DataVersion", outputFormat.dataVersion);
		data.putLong("DayTime", 0L);
		data.putByte("Difficulty", (byte)0);
		data.putByte("DifficultyLocked", (byte)0);
		data.putCompound("DimensionData", new CompoundTag());
		data.putCompound("GameRules", new CompoundTag());
		CompoundTag worldgen = new CompoundTag();
		worldgen.putByte("bonus_chest", (byte) 0);
		worldgen.putLong("seed", oldLevel.getLong("RandomSeed"));
		worldgen.putByte("generate_features", (byte) 0);
		CompoundTag dimensions = new CompoundTag();
		CompoundTag overworld = new CompoundTag();
		CompoundTag flatgen = new CompoundTag();
		CompoundTag settings = new CompoundTag();
		settings.putString("biome", "minecraft:plains");
		settings.putByte("features", (byte) 0);
		settings.putByte("lakes", (byte) 0);
		ListTag<CompoundTag> layers = new ListTag<>();
		if (oldLevel.getString("generatorName").equals("flat") && oldLevel.getString("generatorOptions").length() == 0) {
			// The world generator is a standard flat one
			CompoundTag l1 = new CompoundTag();
			l1.putString("block", "minecraft:bedrock");
			l1.putInt("height", 1);
			layers.add(l1);
			CompoundTag l2 = new CompoundTag();
			l2.putString("block", "minecraft:dirt");
			l2.putInt("height", 2);
			layers.add(l2);
			CompoundTag l3 = new CompoundTag();
			l3.putString("block", "minecraft:grass_block");
			l3.putInt("height", 1);
			layers.add(l3);
		} else {
			// The world generator is custom or noise, set it to void
			CompoundTag air = new CompoundTag();
			air.putString("block", "minecraft:air");
			air.putInt("height", 1);
			layers.add(air);
		}
		settings.put("layers", layers);
		flatgen.putCompound("settings", settings);
		flatgen.putString("type", "minecraft:flat");
		overworld.putCompound("generator", flatgen);
		overworld.putString("type", "minecraft:overworld");
		dimensions.putCompound("minecraft:overworld", overworld);
		worldgen.putCompound("dimensions", dimensions);
		data.putCompound("WorldGenSettings", worldgen);
		data.putInt("GameType", 1);
		data.putInt("version", 19133);
		data.putByte("hardcore", (byte) 0);
		data.putByte("initialized", (byte) 1);
		data.putLong("LastPlayed", oldLevel.getLong("LastPlayed"));
		data.putString("LevelName", oldLevel.getString("LevelName"));
		data.putByte("raining", (byte) 0);
		data.putByte("thundering", (byte) 0);
		data.putInt("rainTime", 1111);
		data.putInt("thunderTime", 1111);
		data.putInt("SpawnX", oldLevel.getInt("SpawnX"));
		data.putInt("SpawnY", oldLevel.getInt("SpawnY"));
		data.putInt("SpawnZ", oldLevel.getInt("SpawnZ"));
		data.putLong("Time", oldLevel.getLong("Time"));
		CompoundTag version = new CompoundTag();
		version.putInt("Id", outputFormat.dataVersion);
		version.putString("Name", "1.15.2");
		version.putByte("Snapshot", (byte) 0);
		data.putCompound("Version", version);
		level.put("Data", data);
		
		return level;
	}
	
	private static CompoundTag convertTile(PalettedBlock block, BlockPosition pos, CompoundTag oldTile) {
		CompoundTag tile = new CompoundTag();
		tile.putInt("x", pos.x);
		tile.putInt("y", pos.y);
		tile.putInt("z", pos.z);
		tile.putByte("keepPacked", (byte) 0);
		
		int id = block.getMaterial().id;
		int meta = block.getMetaValue();
		
		switch (id) {
		case 63:
		case 68:
			tile.putString("id", "minecraft:sign");
			if (oldTile != null) {
				MCAUtil.rewrapSignText(tile, oldTile);
			}
			break;
		case 176:
		case 177:
			tile.putString("id", "minecraft:banner");
			tile.put("Patterns", new ListTag<CompoundTag>());
			
			if (oldTile != null) {
				String type = id == 176 ? "_banner" : "_wall_banner";
				switch (oldTile.getInt("Base")) {
				case 0: block.setName("black" + type); break;
				case 1: block.setName("red" + type); break;
				case 2: block.setName("green" + type); break;
				case 3: block.setName("brown" + type); break;
				case 4: block.setName("blue" + type); break;
				case 5: block.setName("purple" + type); break;
				case 6: block.setName("cyan" + type); break;
				case 7: block.setName("light_gray" + type); break;
				case 8: block.setName("gray" + type); break;
				case 9: block.setName("pink" + type); break;
				case 10: block.setName("lime" + type); break;
				case 11: block.setName("yellow" + type); break;
				case 12: block.setName("light_blue" + type); break;
				case 13: block.setName("magenta" + type); break;
				case 14: block.setName("orange" + type); break;
				case 15: block.setName("white" + type); break;
				}
			}
			break;
		case 144:
			if (oldTile != null) {
				String position = "_wall";
				if (meta == 1) {
					block.set("rotation", oldTile.getByte("Rot"));
					position = "";
				}
				switch (oldTile.getByte("SkullType")) {
				case 0: block.setName("skeleton" + position + "_skull"); break;
				case 1: block.setName("wither_skeleton" + position + "_skull"); break;
				case 2: block.setName("zombie" + position + "_head"); break;
				case 3: block.setName("player" + position + "_head"); break;
				case 4: block.setName("creeper" + position + "_head"); break;
				case 5: block.setName("dragon" + position + "_head"); break;
				}
			}
			break;
		case 26:
			if (oldTile != null) {
				switch (oldTile.getByte("color")) {
				case 1: block.setName("orange_bed"); break;
				case 2: block.setName("magenta_bed"); break;
				case 3: block.setName("light_blue_bed"); break;
				case 4: block.setName("yellow_bed"); break;
				case 5: block.setName("lime_bed"); break;
				case 6: block.setName("pink_bed"); break;
				case 7: block.setName("gray_bed"); break;
				case 8: block.setName("light_gray_bed"); break;
				case 9: block.setName("cyan_bed"); break;
				case 10: block.setName("purple_bed"); break;
				case 11: block.setName("blue_bed"); break;
				case 12: block.setName("brown_bed"); break;
				case 13: block.setName("green_bed"); break;
				case 14: block.setName("red_bed"); break;
				case 15: block.setName("black_bed"); break;
				}
			}
			break;
		case 54:
			tile.putString("id", "minecraft:chest");
			tile.put("Items", new ListTag<CompoundTag>());
			break;
		case 61:
			tile.putString("id", "minecraft:furnace");
			tile.put("Items", new ListTag<CompoundTag>());
			tile.putShort("BurnTime", (short)0);
			tile.putShort("CookTime", (short)0);
			tile.putShort("CookTimeTotal", (short)0);
			tile.putCompound("RecipesUsed", new CompoundTag());
			break;
		case 146:
			tile.putString("id", "minecraft:trapped_chest");
			tile.put("Items", new ListTag<CompoundTag>());
			break;
		case 116: tile.putString("id", "minecraft:enchanting_table"); break;
		case 125:
			tile.putString("id", "minecraft:dropper");
			tile.put("Items", new ListTag<CompoundTag>());
			break;
		case 130:
			tile.putString("id", "minecraft:ender_chest");
			tile.put("Items", new ListTag<CompoundTag>());
			break;
		case 138:
			tile.putString("id", "minecraft:beacon");
			tile.putInt("Levels", 0);
			tile.putInt("Primary", 0);
			tile.putInt("Secondary", 0);
			break;
		case 149:
		case 150:
			tile.putString("id", "minecraft:comparator");
			tile.putInt("OutputSignal", 0);
			break;
		case 151: tile.putString("id", "minecraft:daylight_detector"); break;
		case 154:
			tile.putString("id", "minecraft:hopper");
			tile.put("Items", new ListTag<CompoundTag>());
			tile.putInt("TransferCooldown", 0);
			break;
		case 117:
			tile.putString("id", "minecraft:brewing_stand");
			tile.put("Items", new ListTag<CompoundTag>());
			tile.putShort("BrewTime", (short)0);
			tile.putByte("Fuel", (byte)0);
			break;
		case 23:
			tile.putString("id", "minecraft:dispenser");
			tile.put("Items", new ListTag<CompoundTag>());
			break;
		case 25:
			tile.putString("id", "minecraft:note_block");
			break;
		}
		return tile;
	}

	private static CompoundTag convertItemFrame(BlockPosition realPos, int meta, CompoundTag oldFrameTag) {
		CompoundTag entityTag = MCAUtil.newBaseEntity("minecraft:item_frame");
		ListTag<DoubleTag> pos = new ListTag<>();
		pos.add(new DoubleTag("", realPos.x));
		pos.add(new DoubleTag("", realPos.y));
		pos.add(new DoubleTag("", realPos.z));
		entityTag.put("Pos", pos);
		entityTag.putInt("TileX", realPos.x);
		entityTag.putInt("TileY", realPos.y);
		entityTag.putInt("TileZ", realPos.z);
		switch (meta) {
		case 0: entityTag.putByte("Facing", (byte) 5); break;
		case 1: entityTag.putByte("Facing", (byte) 4); break;
		case 2: entityTag.putByte("Facing", (byte) 3); break;
		case 3: entityTag.putByte("Facing", (byte) 2); break;
		}
		entityTag.putFloat("ItemDropChance", (float)1.0);
		entityTag.putInt("Invisible", 0);
		entityTag.putInt("Fixed", 1);
		if (oldFrameTag != null) {
			CompoundTag itemTag = oldFrameTag.getCompound("Item");
			short itemId = itemTag.getShort("id");
			if (itemId != 0) {
				short itemMeta = itemTag.getShort("Damage");
				String item = getItem(itemId, itemMeta);
				if (item != null) {
					CompoundTag newItemTag = new CompoundTag();
					newItemTag.putByte("Count", (byte)1);
					newItemTag.putString("id", "minecraft:" + item);
					entityTag.putCompound("Item", newItemTag);
					entityTag.putByte("ItemRotation", oldFrameTag.getByte("ItemRotation"));
				} else {
					Bukkit.getLogger().info("Unknown item in item frame: " + itemId + ":" + itemMeta);
				}
			}
		}
		
		return entityTag;
	}
	
	private int[] convertBiomesArray(byte[] oldBiomes, int[] biomeColors) {
		int[] biomes = new int[_1_16_BIOME_SIZE];
		if ((args & ARG_KEEP_BIOMES) != 0) {
			boolean useBiomeColors = false;
			if (oldBiomes == null && biomeColors != null && biomeColors.length == 256) {
				useBiomeColors = true;
			}
			
			for (int x = 0; x < 4; x++) {
				for (int z = 0; z < 4; z++) {
					int biomeId = (useBiomeColors ?
							(biomeColors[z*4*16 + x*4] >> 24) :
							(int)(oldBiomes[z*4*16 + x*4])) & 0xff;
					
					for (int y = 0; y < 64; y++) {
						biomes[getBiomeIndex(x, y, z)] = biomeId;
					}
				}
			}
		} else {
			// Every biome is plains
			Arrays.fill(biomes, 1);
		}
		return biomes;
	}

	private static ListTag<CompoundTag> mapEntities(final CompoundTag chunk) {
		ListTag<? extends Tag> entitiesTag = chunk.getList("Entities");
		ListTag<CompoundTag> entities = new ListTag<>();
		
		for (int i = 0; i < entitiesTag.size(); ++i) {
			CompoundTag entity = (CompoundTag) entitiesTag.get(i);
			if (entity.getString("id").equals("Painting")) {
				CompoundTag painting = MCAUtil.newBaseEntity("minecraft:painting");
				painting.putInt("TileX", entity.getInt("TileX"));
				painting.putInt("TileY", entity.getInt("TileY"));
				painting.putInt("TileZ", entity.getInt("TileZ"));
				
				String motif = entity.getString("Motive");
				/*String newMotif = MCAUtil.PAINTING_MOTIFS.get(motif);
				if (newMotif == null) {
					newMotif = motif.toLowerCase();
				}*/
				painting.putString("Motive", "minecraft:" + motif.toLowerCase());
				
				byte direction = entity.getByte("Direction");
				painting.putByte("Facing", direction);
				
				entities.add(painting);
			}
		}
		
		return entities;
	}
	
	private static Map<Integer, CompoundTag> mapTileEntities(final CompoundTag chunk) {
		ListTag<? extends Tag> oldTilesTag = chunk.getList("TileEntities");
		Map<Integer, CompoundTag> oldTiles = new WeakHashMap<>();
		
		for (int ti = 0; ti < oldTilesTag.size(); ++ti) {
			CompoundTag tile = (CompoundTag) oldTilesTag.get(ti);
			switch (tile.getString("id")) {
			case "Sign":
			case "Banner":
			case "Bed":
			case "FlowerPot":
			case "Skull":
			case "ItemFrame":
				int x = tile.getInt("x");
				int y = tile.getInt("y");
				int z = tile.getInt("z");
				oldTiles.put(getMCRIndex(x & 0xF, y & 0xF, z & 0xF, y >> 4), tile);
				break;
			}
		}
		
		return oldTiles;
	}
	
	static LegacyMaterial getMaterial(int id) {
		LegacyMaterial material = BLOCK_MAP[id];
		if (material != null) return material;
		return BLOCK_MAP[0];
	}
	
	static String getItem(int id, int meta) {
		String item = ITEM_MAP.get((id << 4) | meta);
		if (item == null) {
			if (id < BLOCK_MAP.length) {
				if (BLOCK_MAP[id] != null) {
					LegacyMaterial mat = BLOCK_MAP[id];
					LegacyMetadata metadata = mat.metadata[meta & mat.getMetaMask()];
					if (meta > 0 && metadata != null) {
						item = metadata.namespace;
					} else {
						item = BLOCK_MAP[id].namespace;	
					}
				}
			}
		}
		return item;
	}

	public static void loadBlockTable(JSONParser parser, InputStream stream) throws IOException, ParseException {
		final JSONObject root = (JSONObject) parser.parse(new InputStreamReader(stream, "UTF-8"));
		root.keySet().forEach(key -> {
			Object data = root.get(key);
			int id = Integer.parseInt(key.toString());
			
			if (data instanceof Map) {
				Map dataMap = (Map) data;
				Map metadataMap = (Map) dataMap.get("metadata");
				String baseName = Objects.toString(dataMap.get("name"), null);
				LegacyMetadata[] legacyMeta = new LegacyMetadata[16];
				
				for (Object metaKey : metadataMap.keySet()) {
					Map metaProps = (Map) metadataMap.get(metaKey);
					String metaBlockName = Objects.toString(metaProps.get("name"), null);
					boolean isMetaBit = (boolean) metaProps.getOrDefault("metabit", false);
					metaProps.remove("name");
					metaProps.remove("metabit");
					
					int metaVal = Integer.parseInt(metaKey.toString());
					legacyMeta[metaVal] = new LegacyMetadata(metaBlockName, isMetaBit, metaProps);
				}
				BLOCK_MAP[id] = new LegacyMaterial(id, baseName, legacyMeta);
			} else {
				BLOCK_MAP[id] = new LegacyMaterial(id, data.toString(), null);
			}
		});
		parser.reset();
	}
	
	public static void loadItemTable(JSONParser parser, InputStream stream) throws IOException, ParseException {
		final JSONObject root = (JSONObject) parser.parse(new InputStreamReader(stream, "UTF-8"));
		root.keySet().forEach(key -> {
			String[] data = key.toString().split(":");
			int meta = data.length > 1 ? Integer.parseInt(data[1]) : 0;
			ITEM_MAP.put(((Integer.parseInt(data[0]) << 4) | meta), root.get(key).toString());
		});
		parser.reset();
	}

	@Override
	protected boolean verifyDataVersion(int dataVersion) {
		boolean result = dataVersion <= WorldFormat._1_12.dataVersion;
		if (result == false) {
			log("Chunk is not in old-Anvil format; skipping region.");
		}
		return result;
	}
	
	class PalettedBlock {
		private final LegacyMaterial material;
		private String name;
		private int meta;
		private Map<String, Object> properties = new HashMap<>();
	    /* Coordinates relative to the section */
	    public int relX, relY, relZ;

	    public PalettedBlock(LegacyMaterial material) {
	        this.material = material;
	    }
	    
	    public PalettedBlock(LegacyMaterial material, int meta, int relativeX, int relativeY, int relativeZ) {
	        this.material = material;
	        this.meta = meta;
	        this.relX = relativeX;
	        this.relY = relativeY;
	        this.relZ = relativeZ;

	        int masked = meta & material.getMetaMask();
			LegacyMetadata metadata = material.metadata[masked];
			if (material.namespace != null) {
				this.name = material.namespace;
			}
			
			if (metadata != null) {
				if (metadata.namespace != null) {
					this.name = metadata.namespace;
				}
	   	 		this.properties.putAll(metadata.properties);
			}
			
			if (this.name == null) {
				throw new RuntimeException("Invalid data for block " + material.id + ":" + meta);
			}
			
	        putAllByMetaBit(4);
	        putAllByMetaBit(8);
	    }
	    
	    public void putAllByMetaBit(int metaBit) {
	    	if (meta >= metaBit && material.metadata[metaBit] != null && material.metadata[metaBit].isMetaBit) {
				properties.putAll(material.metadata[metaBit].properties);
			}
	    }

	    public String get(String k) { return properties.get(k).toString(); }
	    public void set(String k, int v) { properties.put(k, v); }
	    public void set(String k, String v) { properties.put(k, v); }
	    public void setName(String name) { this.name = name; }
	    public String getName() { return this.name; }
	    public LegacyMaterial getMaterial() { return this.material; }
	    public int getMetaValue() { return this.meta; }
	    public Map<String, Object> getProperties() { return this.properties; }
	    
	    @Override
	    public String toString() {
	    	StringBuilder str = new StringBuilder(name);
	    	for (Map.Entry<?, ?> entry : properties.entrySet()) {
	    		str.append(";" + entry.getKey() + "=" + String.valueOf(entry.getValue()));
	    	}
	    	return str.toString();
	    }
	}
	
	static class LegacyMaterial {
		public int id;
		public String namespace;
		
		public boolean isSolid = true;
		public boolean hasTile = false;
		public boolean canConnect = false;
		public MaterialType type = MaterialType.NONE;
		public LegacyMetadata[] metadata = new LegacyMetadata[16];
	    
		public LegacyMaterial(int id, @Nullable String baseName, @Nullable LegacyMetadata[] metadata) {
			String namespace = baseName == null ? (metadata[0] == null ? "" : metadata[0].namespace) : baseName;
			Material bukkitMaterial = Material.getMaterial(namespace.toUpperCase(), false);
			if (bukkitMaterial == null) {
				Bukkit.getLogger().warning("Unknown material: " + namespace + " [ID=" + id + "]");
				
				return;
			}
			
			this.id = id;
			this.namespace = baseName;
			if (metadata != null) {
				this.metadata = metadata;
			}
			
			if (org.bukkit.Tag.STAIRS.isTagged(bukkitMaterial)) this.type = MaterialType.STAIR;
			else if (org.bukkit.Tag.FENCES.isTagged(bukkitMaterial)) this.type = MaterialType.FENCE;
			else if (org.bukkit.Tag.DOORS.isTagged(bukkitMaterial)) this.type = MaterialType.DOOR;
			else if (org.bukkit.Tag.FENCE_GATES.isTagged(bukkitMaterial)) this.type = MaterialType.FENCE_GATE;
			else if (namespace.endsWith("glazed_terracotta")) this.type = MaterialType.GLAZED_TERRACOTTA;
			
			if (namespace.endsWith("glass")
			|| bukkitMaterial == Material.GLOWSTONE
			|| bukkitMaterial == Material.REDSTONE_LAMP
			|| org.bukkit.Tag.ICE.isTagged(bukkitMaterial)) {
				this.isSolid = true;
			} else if (org.bukkit.Tag.SLABS.isTagged(bukkitMaterial) && "double".equals(metadata[0].properties.get("type"))) {
				this.isSolid = true;
			} else if (bukkitMaterial == Material.BARRIER) {
				this.isSolid = false;
			} else {
				this.isSolid = bukkitMaterial.isOccluding();
			}
			
			this.hasTile = MCAUtil.TILE_BLOCKS.contains(id);
			if (org.bukkit.Tag.FENCES.isTagged(bukkitMaterial)
			|| org.bukkit.Tag.WALLS.isTagged(bukkitMaterial)
			|| namespace.endsWith("glass_pane")
			|| bukkitMaterial == Material.IRON_BARS) {
				this.canConnect = true;
			}
		}
		
		public boolean is(MaterialType type) {
			return type == this.type;
		}
		
		public boolean canConnect(final LegacyMaterial to) {
			if (this.is(MaterialType.FENCE)) {
				return to.isSolid || to.is(MaterialType.FENCE);
			}
			
			return to.isSolid || (to.canConnect && !to.is(MaterialType.FENCE));
		}

		public int getMetaMask() {
			int mask = 0xF;
			if (metadata[4] != null && metadata[4].isMetaBit) {
				mask = 0x3;
			} else if (metadata[8] != null && metadata[8].isMetaBit) {
				mask = 0x7;
			}
			return mask;
		}

		public enum MaterialType {
			STAIR,
			FENCE,
			FENCE_GATE,
			DOOR,
			GLAZED_TERRACOTTA,
			NONE
		}
	}
	
	private static class LegacyMetadata {
		public final String namespace;
		public final boolean isMetaBit;
		public Map properties = new HashMap();

		public LegacyMetadata(@Nullable String namespace,
							boolean isMetaBit,
							Map properties) {
			this.namespace = namespace;
			this.isMetaBit = isMetaBit;
			if (properties != null) {
				this.properties = properties;
			}
		}
	}
}