package me.ppopovabg.converter.format;

import static me.ppopovabg.converter.format.RegionFormat.*;
import static org.iq80.leveldb.impl.Iq80DBFactory.factory;

import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.compress.utils.BitInputStream;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.iq80.leveldb.CompressionType;
import org.iq80.leveldb.DB;
import org.iq80.leveldb.DBIterator;
import org.iq80.leveldb.Options;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mojang.nbt.CompoundTag;
import com.mojang.nbt.EndTag;
import com.mojang.nbt.ListTag;
import com.mojang.nbt.NbtIo;
import com.mojang.nbt.RegionFile;
import com.mojang.nbt.Tag;

import me.ppopovabg.converter.ConverterPlugin;
import nl.itslars.mcpenbt.NBTUtil;

public class LevelDBReader implements IConverter {
	private static final String inputFolder = "db";
	private final String outputFolder = "region";
	private final String worldPath;
	private final String worldName;
	private final Set<String> unknownBlocks = new HashSet<>();
	
	private boolean isRunning = true;
	private CommandSender commandSender = null;
	
	private static final Map<String, Map> BLOCK_STATES = new HashMap<>();
	
	public LevelDBReader(String worldname) {
		this.worldName = worldname;
		this.worldPath = Bukkit.getWorldContainer() + "/" + worldname;
	}
	
	@Override
	public void start(CommandSender sender) {
		this.commandSender = sender;
		File dbFolder = new File(worldPath, inputFolder);
		String outPath = worldPath + "/" + outputFolder;

		log("Using converter: " + getClass().getName());

		if (!dbFolder.isDirectory()) {
			log(ChatColor.RED + "Folder '" + dbFolder.getPath() + "' does not exist.");
			stop();

			return;
		}
		
		log("Conversion initiated for world '" + worldName + "'. Regions will be written to /" + outputFolder);
		log("Output format: " + ChatColor.AQUA + WorldFormat.ANVIL.name);
		
		try {
			new File(outPath).mkdir();
			
			convertLevel(dbFolder, outPath);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			stop();
		}
	}

	public void convertLevel(final File dbFolder, final String outPath) throws IOException {
		Options options = new Options()
				.createIfMissing(true)
				.compressionType(CompressionType.ZLIB_RAW)
				.blockSize(64 * 1024);

		final DB db = factory.open(dbFolder, options);
		Set<SimpleImmutableEntry> presentChunks = new LinkedHashSet<>();

		log("Counting chunks...");
		try (DBIterator iterator = db.iterator()) {
			iterator.seekToFirst();
			while (iterator.hasNext()) {
				Map.Entry<?,?> entry = iterator.next();
				byte[] key = (byte[]) entry.getKey();
				String keyName = new String(key);

				if (!keyName.matches("^[a-zA-Z]*$") && key.length >= 8 && key.length <= 14) {
					int chunkX = ByteBuffer.wrap(key, 0, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
					int chunkZ = ByteBuffer.wrap(key, 4, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();

					if (key.length > 10) {
						int dimensionID = ByteBuffer.wrap(key, 8, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
						if (dimensionID != -1) {
							continue;
						}
					}

					presentChunks.add(new SimpleImmutableEntry(chunkX, chunkZ));
				}
			}
		}

		RegionFile regionDest = null;
		int regionX = -1;
		int regionZ = -1;
		
		final int chunkCount = presentChunks.size();
		int processedChunksCount = 0;
		for (SimpleImmutableEntry chunkPosition : presentChunks) {
			if (!isRunning) {
				System.out.println(ChatColor.GRAY + "Conversion has been forcibly stopped.");
				break;
			}

			int chunkX = (int) chunkPosition.getKey();
			int chunkZ = (int) chunkPosition.getValue();
			int currentRegionX = chunkX >> 5;
			int currentRegionZ = chunkZ >> 5;

			processedChunksCount++;
			if (regionX != currentRegionX || regionZ != currentRegionZ) {
				if (regionDest != null) {
					regionDest.close();
				}
				
				regionDest = new RegionFile(new File(worldPath,
						outputFolder + "/r." + currentRegionX + "." + currentRegionZ + ".mca"));
				regionX = currentRegionX;
				regionZ = currentRegionZ;

				log(String.format("Written " + ChatColor.GRAY + "r.%d.%d.mca [%d/%d]", regionX, regionZ, processedChunksCount, chunkCount));
			}

			CompoundTag tag = convertChunkNBT(db, chunkX, chunkZ);
			DataOutputStream chunkDataOutputStream = regionDest.getChunkDataOutputStream(chunkX & 31, chunkZ & 31);
			NbtIo.write(tag, chunkDataOutputStream);
			chunkDataOutputStream.close();
		}

		db.close();
		if (regionDest != null) {
			regionDest.close();
		}

		log(ChatColor.GREEN + "Conversion completed.");

		if (unknownBlocks.size() > 0) {
			log(ChatColor.GRAY + "The following blocks were not found in the block ID table: ("
					+ String.join(", ", unknownBlocks) + ")");
		}
	}

	private CompoundTag convertChunkNBT(final DB db, int chunkX, int chunkZ) {
		CompoundTag root = new CompoundTag();
		root.putInt("DataVersion", _1_15_DATAVERSION);

		CompoundTag chunk = MCAConverter.emptyChunkNBT(chunkX, chunkZ);
		ListTag<CompoundTag> sections = new ListTag<>();
		
		Map<Byte, List> ldbChunkTiles = new HashMap<>();
		ListTag<? extends Tag> tileEntities = mapTileEntities(db.get(getLevelDBKey(chunkX, chunkZ, LDB_TILES_PREFIX)), ldbChunkTiles);
		
		for (byte subChunkHeight = 0; subChunkHeight < 16; subChunkHeight++) {
			byte[] sectionKey = getLevelDBKey(chunkX, chunkZ, subChunkHeight, LDB_SUBCHUNK_PREFIX);
			byte[] value = db.get(sectionKey);

			if (value == null) {
				continue;
			}

			try (InputStream inputStream = new ByteArrayInputStream(value)) {
				int version = inputStream.read();
				int storageCount = 1;
				if (version == 8) {
					storageCount = inputStream.read();
				}

				List<CompoundTag> palette = new ArrayList<>();
				long[] blockStates = null;
				for (int blockStorage = 0; blockStorage < storageCount; blockStorage++) {
					byte storageVersion = (byte) inputStream.read();
					int bitsPerBlock = storageVersion >> 1;
					int blocksPerWord = (int) Math.floor(32.0 / bitsPerBlock);
					int wordCount = (int) Math.ceil(4096.0 / blocksPerWord);
					boolean hasPadding = 32 % blocksPerWord != 0;

					int position = 0;
					BitInputStream bitInputStream = new BitInputStream(inputStream, ByteOrder.LITTLE_ENDIAN);
					short[] blocks = new short[BLOCK_ARR_SIZE];
					for (int wordIndex = 0; wordIndex < wordCount; wordIndex++) {
						for (int blockIndex = 0; blockIndex < blocksPerWord; blockIndex++) {
							long result = bitInputStream.readBits(bitsPerBlock);
							if (position >= BLOCK_ARR_SIZE) {
								continue;
							}
							blocks[position] = (short) result;

							position++;
						}
						if (hasPadding) {
							bitInputStream.readBits(2);
						}
					}
					bitInputStream.close();
					
					if (blockStorage == 0) {
						// Block palette storage
						int paletteSize = ByteBuffer.wrap(new byte[] { (byte) inputStream.read(), (byte) inputStream.read(),
										(byte) inputStream.read(), (byte) inputStream.read() })
										.order(ByteOrder.LITTLE_ENDIAN).getInt();
						
						for (int i = 0; i < paletteSize; i++) {
							nl.itslars.mcpenbt.tags.CompoundTag ldbBlock = (nl.itslars.mcpenbt.tags.CompoundTag) NBTUtil.read(false, inputStream);
							String name = ldbBlock.getByName("name").get().getAsString().getValue();
							CompoundTag anvilBlock = translateBlock(ldbBlock);
							
							if (anvilBlock == null) {
								anvilBlock = new CompoundTag();
								anvilBlock.putString("Name", "minecraft:air");
								unknownBlocks.add(name);
							}
							
							palette.add(anvilBlock);
						}

						List<nl.itslars.mcpenbt.tags.CompoundTag> sectionTiles = ldbChunkTiles.get(subChunkHeight);
						if (sectionTiles != null) {
							for (nl.itslars.mcpenbt.tags.CompoundTag tile : sectionTiles) {
								int x = tile.getByName("x").get().getAsInt().getValue();
								int y = tile.getByName("y").get().getAsInt().getValue();
								int z = tile.getByName("z").get().getAsInt().getValue();
								int blockstateIndex = getLevelDBIndex(x & 0xF, y & 0xF, z & 0xF);
	                        	int paletteIndex = blocks[blockstateIndex];
								CompoundTag baseBlock = (CompoundTag) palette.get(paletteIndex).copy();
								
								adjustBlockEntity(tile, baseBlock);
								
								if (!palette.contains(baseBlock)) {
									palette.add(baseBlock);
									paletteIndex = palette.size() - 1;
								} else {
									paletteIndex = palette.indexOf(baseBlock);
								}
								blocks[blockstateIndex] = (short) paletteIndex;
							}
							paletteSize = palette.size();
						}
						
						if (paletteSize > 16) {
							final int paletteIndexSize = Math.max(4, (int) Math.ceil(Math.log(paletteSize) / Math.log(2)));
							blockStates = new long[64 * paletteIndexSize];
						} else {
							blockStates = new long[256];
						}
						
						for (int x = 0; x < 16; x++) {
                            for (int y = 0; y < 16; y++) {
                                for (int z = 0; z < 16; z++) {
                                	int paletteIndex = blocks[getLevelDBIndex(x, y, z)];
                                	if (paletteIndex >= paletteSize) {
                                		//System.out.printf(ChatColor.YELLOW + "PaletteIndex was bigger than size of palette (%d) at %d, %d, %d in CX:%d, SY:%d, CZ:%d\n", paletteIndex, x, y, z, chunkX, subChunkHeight, chunkZ);
                                		continue;
                                	}
                                	
									setPaletteIndex(_1_15_DATAVERSION,
											getBlockstateIndex(x, y, z), paletteIndex, blockStates);
								}
							}
						}
						
						if (storageCount == 1) {
							ListTag<CompoundTag> paletteTag = new ListTag<>();
							palette.stream().forEach(x -> paletteTag.add(x));
	    					sections.add(newSection(subChunkHeight, blockStates, paletteTag));
						}
					} else if (blockStorage == 1) {
						// Waterlogging storage
						Set<Integer> waterloggingIndeces = new HashSet<>();
                        for (int x = 0; x < 16; x++) {
                            for (int y = 0; y < 16; y++) {
                                for (int z = 0; z < 16; z++) {
                                    short waterloggingPaletteIndex = blocks[getLevelDBIndex(x, y, z)];
                                    if (waterloggingPaletteIndex == 1) {
                                        waterloggingIndeces.add(getPaletteIndex(_1_15_DATAVERSION, getBlockstateIndex(x, y, z), blockStates));
                                    }
                                }
                            }
                        }

                        ListTag<CompoundTag> waterloggedPalette = new ListTag<>();
						for (int i = 0; i < palette.size(); i++) {
							CompoundTag block = palette.get(i);
							if (!block.getString("Name").equals("minecraft:air") && waterloggingIndeces.contains(i)) {
								block.getCompound("Properties").putString("waterlogged", "true");
							}
							waterloggedPalette.add(block);
                        }

    					sections.add(newSection(subChunkHeight, blockStates, waterloggedPalette));
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		chunk.put("Sections", sections);
		
		chunk.put("TileEntities", tileEntities);
		chunk.put("Entities", convertEntities(db.get(getLevelDBKey(chunkX, chunkZ, LDB_ENTITIES_PREFIX))));
		
		root.putCompound("Level", chunk);

		return root;
	}

	private static CompoundTag translateBlock(final nl.itslars.mcpenbt.tags.CompoundTag ldbBlock) {
		CompoundTag block = new CompoundTag();
		String namespace = ldbBlock.getByName("name").get().getAsString().getValue();
		String name = namespace.replace("minecraft:", "");
		
		Optional<nl.itslars.mcpenbt.tags.Tag> states = ldbBlock.getByName("states");
		if (states.isPresent()) {
			CompoundTag propertiesTag = new CompoundTag();
			
			final List<nl.itslars.mcpenbt.tags.Tag> stateTags = states.get().getAsCompound().getElements();
			Map<String, String> mappedStates = BLOCK_STATES.get(namespace);
			if (mappedStates != null) {
				String newNamespace = mappedStates.get("name");
		
				for (nl.itslars.mcpenbt.tags.Tag stateTag : stateTags) {
					String propName = stateTag.getName();
					String propValue = null;
					if (stateTag instanceof nl.itslars.mcpenbt.tags.ByteTag) {
						propValue = stateTag.getAsByte().getValue() == 1 ? "true" : "false";
					} else if (stateTag instanceof nl.itslars.mcpenbt.tags.StringTag) {
						propValue = stateTag.getAsString().getValue();
					} else {
						propValue = String.valueOf(stateTag.getAsInt().getValue());
					}
					
					if ((propValue.equals("false") && !propName.equals("lit")) || propValue.equals("none")) {
						continue;
					}
		
					String state = mappedStates.get(propName + "=" + propValue);
					if (state != null) {
						if (state.indexOf("=") == -1) {
							newNamespace = state;
						} else {
							for (String prop : state.split(";")) {
								if (prop.indexOf("=") == -1) {
									newNamespace = prop;
								} else {
									propertiesTag.putString(prop.split("=")[0], prop.split("=")[1]);
								}
							}
						}
					}
				}
				name = newNamespace;
			}
			
			if (!propertiesTag.isEmpty()) {
				block.putCompound("Properties", propertiesTag);
			}
		}
	
		Material bukkitMaterial = Material.getMaterial(name.toUpperCase(), false);
		if (bukkitMaterial == null) {
			return null;
		}
	
		block.putString("Name", "minecraft:" + name);
		
		return block;
	}
	
	private static CompoundTag newSection(int subChunkHeight, long[] blockStates, ListTag palette) {
		CompoundTag section = new CompoundTag();
		section.putByte("Y", (byte) subChunkHeight);
		section.putLongArray("BlockStates", blockStates);
		section.put("Palette", palette);
		return section;
	}
	
	private static void adjustBlockEntity(nl.itslars.mcpenbt.tags.CompoundTag tile, CompoundTag block) {
		switch (tile.getByName("id").get().getAsString().getValue()) {
		case "Banner":
			String type = block.getString("Name").replace("minecraft:black", "");
			switch (tile.getByName("Base").get().getAsInt().getValue()) {
			case 0: block.putString("Name", "black" + type); break;
			case 1: block.putString("Name", "red" + type); break;
			case 2: block.putString("Name", "green" + type); break;
			case 3: block.putString("Name", "brown" + type); break;
			case 4: block.putString("Name", "blue" + type); break;
			case 5: block.putString("Name", "purple" + type); break;
			case 6: block.putString("Name", "cyan" + type); break;
			case 7: block.putString("Name", "light_gray" + type); break;
			case 8: block.putString("Name", "gray" + type); break;
			case 9: block.putString("Name", "pink" + type); break;
			case 10: block.putString("Name", "lime" + type); break;
			case 11: block.putString("Name", "yellow" + type); break;
			case 12: block.putString("Name", "light_blue" + type); break;
			case 13: block.putString("Name", "magenta" + type); break;
			case 14: block.putString("Name", "orange" + type); break;
			case 15: block.putString("Name", "white" + type); break;
			}
			break;
		case "FlowerPot":
			Optional<nl.itslars.mcpenbt.tags.Tag> plantOptional = tile.getByName("PlantBlock");
			if (plantOptional.isPresent()) {
				nl.itslars.mcpenbt.tags.CompoundTag plantBlock = plantOptional.get().getAsCompound();
				String name = plantBlock.getByName("name").get().getAsString().getValue();
				if (!name.isEmpty()) {
					CompoundTag translated = translateBlock(plantBlock);
					block.putString("Name", "potted_" + translated.getString("Name").replace("minecraft:", ""));
				}
			}
			break;
		case "Bed":
			switch (tile.getByName("color").get().getAsByte().getValue()) {
			case 1: block.putString("Name", "orange_bed"); break;
			case 2: block.putString("Name", "magenta_bed"); break;
			case 3: block.putString("Name", "light_blue_bed"); break;
			case 4: block.putString("Name", "yellow_bed"); break;
			case 5: block.putString("Name", "lime_bed"); break;
			case 6: block.putString("Name", "pink_bed"); break;
			case 7: block.putString("Name", "gray_bed"); break;
			case 8: block.putString("Name", "light_gray_bed"); break;
			case 9: block.putString("Name", "cyan_bed"); break;
			case 10: block.putString("Name", "purple_bed"); break;
			case 11: block.putString("Name", "blue_bed"); break;
			case 12: block.putString("Name", "brown_bed"); break;
			case 13: block.putString("Name", "green_bed"); break;
			case 14: block.putString("Name", "red_bed"); break;
			case 15: block.putString("Name", "black_bed"); break;
			}
			break;
		}
	}

	private static CompoundTag convertTile(nl.itslars.mcpenbt.tags.CompoundTag ldbTile) {
		String id = ldbTile.getByName("id").get().getAsString().getValue();
		
		switch (id) {
		case "Skull":
		case "FlowerPot":
		case "NoteBlock":
		case "Cauldron":
			return null;
		}

		String tileId = null;
		CompoundTag anvilTile = convertAllTags(ldbTile);
		
		switch (id) {
		case "Chest":
			Optional<nl.itslars.mcpenbt.tags.Tag> itemsOptional = ldbTile.getByName("Items");
			if (!itemsOptional.isPresent()) {
				break;
			}
			
			nl.itslars.mcpenbt.tags.ListTag<nl.itslars.mcpenbt.tags.Tag> ldbItems = itemsOptional.get().getAsList();
			ListTag<CompoundTag> anvilItems = new ListTag<>();

			ldbItems.forEach(tag -> {
				nl.itslars.mcpenbt.tags.CompoundTag item = (nl.itslars.mcpenbt.tags.CompoundTag) tag;
				String name = item.getByName("Name").get().getAsString().getValue();

				CompoundTag itemTag = new CompoundTag();
				itemTag.putByte("Count", item.getByName("Count").get().getAsByte().getValue());
				itemTag.putByte("Slot", item.getByName("Slot").get().getAsByte().getValue());

				if (item.getByName("Block").isPresent()) {
					CompoundTag blockTag = translateBlock(item.getByName("Block").get().getAsCompound());
					itemTag.putString("id", blockTag.getString("Name"));
				} else {
					itemTag.putString("id", name);
				}
				anvilItems.add(itemTag);
			});

			anvilTile.put("Items", anvilItems);
			tileId = "chest";
			break;
		case "BrewingStand":
			tileId = "brewing_stand";
			break;
		case "EnchTable":
			tileId = "enchanting_table";
			break;
		case "TrappedChest":
			tileId = "trapped_chest";
			break;
		case "EnderChest":
			tileId = "ender_chest";
			break;
		case "CommandBlock":
			tileId = "command_block";
			break;
		case "MobSpawner":
			tileId = "mob_spawner";
			break;
		case "BlastFurnace":
			tileId = "blast_furnace";
			break;
		default:
			tileId = id.toLowerCase();
			break;
		}

		anvilTile.putString("id", "minecraft:" + tileId);
		return anvilTile;
	}
	
	private static<T extends nl.itslars.mcpenbt.tags.Tag & Iterable> CompoundTag convertAllTags(T nbt) {
		CompoundTag anvilNbt = new CompoundTag();
		nbt.forEach(e -> {
			T tag = (T) e;
			if (tag instanceof nl.itslars.mcpenbt.tags.StringTag) {
				anvilNbt.putString(tag.getName(), tag.getAsString().getValue());
			} else if (tag instanceof nl.itslars.mcpenbt.tags.IntTag) {
				anvilNbt.putInt(tag.getName(), tag.getAsInt().getValue());
			} else if (tag instanceof nl.itslars.mcpenbt.tags.FloatTag) {
				anvilNbt.putFloat(tag.getName(), tag.getAsFloat().getValue());
			} else if (tag instanceof nl.itslars.mcpenbt.tags.ByteTag) {
				anvilNbt.putByte(tag.getName(), tag.getAsByte().getValue());
			} else if (tag instanceof nl.itslars.mcpenbt.tags.CompoundTag) {
				anvilNbt.putCompound(tag.getName(), convertAllTags(tag.getAsCompound()));
			} else if (tag instanceof nl.itslars.mcpenbt.tags.ListTag) {
				anvilNbt.put(tag.getName(), convertAllTags(tag.getAsList()));
			}
		});
		return anvilNbt;
	}

	private static ListTag<? extends Tag> mapTileEntities(byte[] value, Map<Byte, List> oldTiles) {
		if (value == null) {
			return new ListTag<EndTag>();
		}
		
		ListTag<CompoundTag> tiles = new ListTag<>();
		try (ByteArrayInputStream stream = new ByteArrayInputStream(value)) {
			while (stream.available() > 0) {
				nl.itslars.mcpenbt.tags.CompoundTag ldbTile = (nl.itslars.mcpenbt.tags.CompoundTag) NBTUtil.read(false, stream);
				
				switch (ldbTile.getByName("id").get().getAsString().getValue()) {
				case "Sign":
				case "Banner":
				case "Bed":
				case "FlowerPot":
				case "Skull":
				case "ItemFrame":
					byte sectionY = (byte) (ldbTile.getByName("y").get().getAsInt().getValue() >> 4);
                    List sectionTiles = oldTiles.get(sectionY);
                    if (sectionTiles == null) {
                    	sectionTiles = new ArrayList();
                    }
                    sectionTiles.add(ldbTile);
					oldTiles.put(sectionY, sectionTiles);
					break;
				}
				
				CompoundTag translated = convertTile(ldbTile);
				if (translated != null) {
					tiles.add(translated);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return tiles;
	}

	private static ListTag<? extends Tag> convertEntities(byte[] value) {
		if (value == null) {
			return new ListTag<EndTag>();
		}
		
		ListTag<CompoundTag> entities = new ListTag<>();
		try (ByteArrayInputStream stream = new ByteArrayInputStream(value)) {
			while (stream.available() > 0) {
				nl.itslars.mcpenbt.tags.CompoundTag ldbEntity = (nl.itslars.mcpenbt.tags.CompoundTag)
						NBTUtil.read(false, stream);

				if (ldbEntity.getByName("identifier").get().getAsString().getValue().equals("minecraft:painting")) {
					for (nl.itslars.mcpenbt.tags.Tag tag : ldbEntity) {
						// Dir
						// Direction
						// Variant
						// definitions
						System.out.println(tag.getName() + " " + tag.getType());
					}
					System.out.println(ldbEntity.getByName("Variant").get().getAsString().getValue());
					
					CompoundTag anvilEntity = MCAUtil.newBaseEntity("minecraft:painting");
					/*anvilEntity.putInt("TileX", entity.getInt("TileX"));
					anvilEntity.putInt("TileY", entity.getInt("TileY"));
					anvilEntity.putInt("TileZ", entity.getInt("TileZ"));*/
					entities.add(anvilEntity);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return entities;
	}

	public static void loadBlockTable(JSONParser parser, InputStream stream) throws UnsupportedEncodingException, IOException, ParseException {
		final JSONObject root = (JSONObject) parser.parse(new InputStreamReader(stream, "UTF-8"));
		root.keySet().forEach(key -> {
			BLOCK_STATES.put(key.toString(), (Map<String, String>)root.get(key));
		});
		parser.reset();
	}

	@Override
	public void stop() {
		this.isRunning = false;
		
		ConverterPlugin.instance.stopProcessor(this);
	}

	@Override
	public void log(String message) {
		String formatted = String.format("[%s] %s", worldName, message);
		
		if (commandSender instanceof Player) {
			commandSender.sendMessage(formatted);
		}

		Bukkit.getLogger().info(formatted);
	}

	@Override
	public String getWorldName() {
		return this.worldName;
	}
}
