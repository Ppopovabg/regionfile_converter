package me.ppopovabg.converter.format;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;

import org.bukkit.block.BlockFace;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mojang.nbt.CompoundTag;
import com.mojang.nbt.DoubleTag;
import com.mojang.nbt.FloatTag;
import com.mojang.nbt.ListTag;

import me.ppopovabg.converter.format.MCAConverter.LegacyMaterial;
import me.ppopovabg.converter.format.MCAConverter.PalettedBlock;
import me.ppopovabg.converter.format.MCAConverter.LegacyMaterial.MaterialType;

public final class MCAUtil {
	public static final Set<Integer> TILE_BLOCKS = new HashSet<>(Arrays.asList(25, 54, 146, 117, 63, 68, 23, 26, 61, 116, 125, 130, 138, 149, 150, 151, 154, 144, 176, 177/*, 140*/));
	public static final List<BlockFace> DIRECT_BLOCK_FACES = Arrays.asList(BlockFace.EAST, BlockFace.WEST, BlockFace.SOUTH, BlockFace.NORTH);
	public static final Map<Character, Integer> SIGN_CHARACTER_WIDTHS = new HashMap<>();
	
	static void calculateStairCorner(PalettedBlock stair, BaseBlock e, BaseBlock w,
			BaseBlock s, BaseBlock n) {
		LegacyMaterial east = MCAConverter.getMaterial(e.id);
		LegacyMaterial west = MCAConverter.getMaterial(w.id);
		LegacyMaterial south = MCAConverter.getMaterial(s.id);
		LegacyMaterial north = MCAConverter.getMaterial(n.id);
		
		int meta = stair.getMetaValue();
		switch (meta & 0x3) {
		case 0: // east
			if (east.is(MaterialType.STAIR) && (e.meta & 0x4) == (meta & 0x4)) {
				if ((e.meta & 0x3) == 2) {
					if (!(north.is(MaterialType.STAIR) && n.meta == meta)) {
						stair.set("shape", "outer_right"); break;
					}
				} else if ((e.meta & 0x3) == 3) {
					if (!(south.is(MaterialType.STAIR) && s.meta == meta)) {
						stair.set("shape", "outer_left"); break;
					}
				}
			}
			if (west.is(MaterialType.STAIR) && (w.meta & 0x4) == (meta & 0x4)) {
				if ((w.meta & 0x3) == 2) {
					if (!(south.is(MaterialType.STAIR) && s.meta == meta)) {
						stair.set("shape", "inner_right");
					}
				} else if ((w.meta & 0x3) == 3) {
					if (!(north.is(MaterialType.STAIR) && n.meta == meta)) {
						stair.set("shape", "inner_left");
					}
				}
			}
			break;
		case 1: // west
			if (west.is(MaterialType.STAIR) && (w.meta & 0x4) == (meta & 0x4)) {
				if ((w.meta & 0x3) == 2) {
					if (!(north.is(MaterialType.STAIR) && n.meta == meta)) {
						stair.set("shape", "outer_left"); break;
					}
				} else if ((w.meta & 0x3) == 3) {
					if (!(south.is(MaterialType.STAIR) && s.meta == meta)) {
						stair.set("shape", "outer_right"); break;
					}
				}
			}
			if (east.is(MaterialType.STAIR) && (e.meta & 0x4) == (meta & 0x4)) {
				if ((e.meta & 0x3) == 2) {
					if (!(south.is(MaterialType.STAIR) && s.meta == meta)) {
						stair.set("shape", "inner_left");
					}
				} else if ((e.meta & 0x3) == 3) {
					if (!(north.is(MaterialType.STAIR) && n.meta == meta)) {
						stair.set("shape", "inner_right");
					}
				}
			}
			break;
		case 2: // south
			if (south.is(MaterialType.STAIR) && (s.meta & 0x4) == (meta & 0x4)) {
				if ((s.meta & 0x3) == 0) {
					if (!(west.is(MaterialType.STAIR) && w.meta == meta)) {
						stair.set("shape", "outer_left"); break;
					}
				} else if ((s.meta & 0x3) == 1) {
					if (!(east.is(MaterialType.STAIR) && e.meta == meta)) {
						stair.set("shape", "outer_right"); break;
					}
				}
			}
			if (north.is(MaterialType.STAIR) && (n.meta & 0x4) == (meta & 0x4)) {
				if ((n.meta & 0x3) == 0) {
					if (!(east.is(MaterialType.STAIR) && e.meta == meta)) {
						stair.set("shape", "inner_left");
					}
				} else if ((n.meta & 0x3) == 1) {
					if (!(west.is(MaterialType.STAIR) && w.meta == meta)) {
						stair.set("shape", "inner_right");
					}
				}
			}
			break;
		case 3: // north
			if (north.is(MaterialType.STAIR) && (n.meta & 0x4) == (meta & 0x4)) {
				if ((n.meta & 0x3) == 0) {
					if (!(west.is(MaterialType.STAIR) && w.meta == meta)) {
						stair.set("shape", "outer_right"); break;
					}
				} else if ((n.meta & 0x3) == 1) {
					if (!(east.is(MaterialType.STAIR) && e.meta == meta)) {
						stair.set("shape", "outer_left"); break;
					}
				}
			}
			if (south.is(MaterialType.STAIR) && (s.meta & 0x4) == (meta & 0x4)) {
				if ((s.meta & 0x3) == 0) {
					if (!(east.is(MaterialType.STAIR) && e.meta == meta)) {
						stair.set("shape", "inner_right");
					}
				} else if ((s.meta & 0x3) == 1) {
					if (!(west.is(MaterialType.STAIR) && w.meta == meta)) {
						stair.set("shape", "inner_left");
					}
				}
			}
			break;
		}
	}

	static int rotateBlockFace(int face) {
		int result = 0;
		switch (face) {
		case 3:
			result = 2;
			break;
		case 0:
			result = 1;
			break;
		case 2:
			result = 3;
			break;
		case 1:
			result = 0;
			break;
		}
		return result;
	}
	
	static void rewrapSignText(CompoundTag sign, CompoundTag oldSign) {
		int lineIndex = 1;
		String[] lines = new String[4];
		if (oldSign.contains("Text")) {
			lines = oldSign.getString("Text").split("\n");
		} else {
			// MCRegion
			for (int i=0; i < 4; ++i) {
				lines[i] = oldSign.getString("Text" + (i+1));
			}
		}
		for (String line : lines) {
			if (line.equals("null") || line.trim().length() == 0) {
				// Blank line
				sign.putString("Text" + lineIndex++, new String());
				continue;
			}
			int lineWidth = calculateSignStringWidth(line);
			if (lineWidth > 90) {
				// Text needs to be wrapped to the next line
				int newWidth = 0;
				String[] words = line.split(" ");
				StringJoiner str = new StringJoiner(" ");
				for (String word : words) {
					int wordWidth = calculateSignStringWidth(word);
					int spaceWidth = 4 + wordWidth;
					if ((newWidth + spaceWidth) < 90) {
						newWidth += spaceWidth;
						str.add(word);
					} else {
						newWidth = wordWidth;
						str.add("\n" + word);
					}
				}
				String[] newLines = str.toString().split("\n");
				for (String newLine : newLines) {
					// Sign text tags contain JSON text
					if (newLine.indexOf("\"") == 0) {
						sign.putString("Text" + lineIndex++, newLine);
					} else {
						sign.putString("Text" + lineIndex++, "\"" + newLine + "\"");
					}
				}
			} else {
				if (line.indexOf("\"") == 0) {
					sign.putString("Text" + lineIndex++, line);
				} else {
					sign.putString("Text" + lineIndex++, "\"" + line + "\"");
				}
			}
		}
	}

	private static int calculateSignStringWidth(String text) {
		int width = 0;
		for (char ch : text.toCharArray()) {
			width += SIGN_CHARACTER_WIDTHS.getOrDefault(ch, 6);
		}
		return width;
	}
	
	public static void loadSignCharList(JSONParser parser, InputStream stream) throws IOException, ParseException {
		final JSONObject root = (JSONObject) parser.parse(new InputStreamReader(stream, "UTF-8"));
		root.keySet().forEach(key -> {
			SIGN_CHARACTER_WIDTHS.put(key.toString().charAt(0), ((Long) root.get(key)).intValue());
		});
		parser.reset();
		// Section sign
		SIGN_CHARACTER_WIDTHS.put("\u00a7".charAt(0), 8);
	}
	
	public static final class BlockPosition {
		public int x, y, z;
	    
	    public BlockPosition(int x, int y, int z) {
	        this.x = x;
	        this.y = y;
	        this.z = z;
	    }
	}
	
	public static final class BaseBlock {
		public int id, meta;
	    
	    public BaseBlock(int id, int meta) {
	        this.id = id;
	        this.meta = meta;
	    }
	}
	
	static CompoundTag newBaseEntity(String entityId) {
		CompoundTag entity = new CompoundTag();
		entity.putString("id", entityId);
		entity.putShort("Fire", (short) 0);
		entity.putShort("Air", (short) 300);
		entity.putBoolean("OnGround", true);
		ListTag<DoubleTag> pos = new ListTag<>();
		pos.add(new DoubleTag("", 0));
		pos.add(new DoubleTag("", 0));
		pos.add(new DoubleTag("", 0));
		entity.put("Pos", pos);
		ListTag<DoubleTag> mot = new ListTag<>();
		mot.add(new DoubleTag("", 0));
		mot.add(new DoubleTag("", 0));
		mot.add(new DoubleTag("", 0));
		entity.put("Motion", mot);
		ListTag<FloatTag> rot = new ListTag<>();
		rot.add(new FloatTag("", 0));
		rot.add(new FloatTag("", 0));
		entity.put("Rotation", rot);
		
		return entity;
	}
}
