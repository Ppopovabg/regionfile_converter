package me.ppopovabg.converter.format;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.boydti.fawe.object.RunnableVal;
import com.boydti.fawe.util.TaskManager;
import com.mojang.nbt.CompoundTag;
import com.mojang.nbt.FloatTag;
import com.mojang.nbt.ListTag;
import com.mojang.nbt.NbtIo;
import com.sk89q.jnbt.Tag;
import com.sk89q.worldedit.EmptyClipboardException;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.bukkit.BukkitPlayer;
import com.sk89q.worldedit.entity.BaseEntity;
import com.sk89q.worldedit.entity.Entity;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.math.Vector3;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.registry.state.Property;
import com.sk89q.worldedit.world.block.BaseBlock;

import me.ppopovabg.converter.ConverterPlugin;
import me.ppopovabg.converter.GeyserHook;

/**
 * Converts MC Java 1.13+ schematic format to FAWE Nukkit schematic format and uploads it
 */
public class NukkitSchemConverter {
	private static final String SCHEMATIC_HOST = "https://file.io/?expires=1d";
	private static final byte[] CRLF = "\r\n".getBytes();
	private static final String PLATFORM = "nukkit";
	private static final int MAX_VOLUME = 512 * 512 * 256;
	
	private static String save(UUID uuid, CompoundTag tag) throws IOException {
		String response = null;
		final File temp = new File(ConverterPlugin.instance.getDataFolder(), uuid.toString());

		temp.getParentFile().mkdirs();

		try {
			BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(temp));
			NbtIo.writeCompressed(tag, outputStream);
			outputStream.close();

			response = upload(temp, "converted.schematic");

		} catch (IOException e) {
			throw e;
		} finally {
			temp.delete();
		}

		return response;
	}

	private static String upload(File file, String filename) {
		String response = null;

		try {
			final URL url = new URL(SCHEMATIC_HOST);
			final String boundary = Long.toHexString(System.currentTimeMillis());
			final HttpURLConnection con = (HttpURLConnection) url.openConnection();

			con.setRequestMethod("POST");
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
			
			try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
				output.write(("--" + boundary + "\r\n").getBytes());
				output.write(("Content-Disposition: form-data; name=\"param\"").getBytes());
				output.write(CRLF);
				output.write(("Content-Type: text/plain; charset=" + StandardCharsets.UTF_8.displayName()).getBytes());
				output.write(CRLF);
				output.write(CRLF);
				output.write(("value").getBytes());
				output.write(CRLF);

				output.write(("--" + boundary + "\r\n").getBytes());
				output.write(("Content-Disposition: form-data; name=\"file\"; filename=\""+ filename +"\"").getBytes());
				output.write(CRLF);
				output.write(("Content-Type: application/octet-stream").getBytes());
				output.write(CRLF);
				output.write(("Content-Transfer-Encoding: binary").getBytes());
				output.write(CRLF);
				output.write(CRLF);

				BufferedInputStream fileStream = new BufferedInputStream(new FileInputStream(file));
				while (fileStream.available() > 0) {
					output.write(fileStream.read());
				}
				fileStream.close();

				output.write(CRLF);
				output.write(("--" + boundary + "--").getBytes());

				final byte[] data = output.toByteArray();
				OutputStream request = con.getOutputStream();
				request.write(data);
				request.flush();
				request.close();
			}

			int status = con.getResponseCode();
			InputStream responseStream = null;
			switch (status) {
			case 200:
			case 201:
				responseStream = con.getInputStream();
				break;
			default:
				responseStream = con.getErrorStream();
				break;
			}

			BufferedReader reader = new BufferedReader(new InputStreamReader(responseStream));
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line + "\n");
			}
			reader.close();

			response = builder.toString();

		} catch (IOException e) {
			response = e.toString();
			e.printStackTrace();
		}

		return response;
	}
	
	private static void write(final Clipboard clipboard, final RunnableVal<CompoundTag> done) {
		TaskManager.IMP.async(() -> {
			final Region region = clipboard.getRegion();
			BlockVector3 origin = clipboard.getOrigin();
			BlockVector3 min = region.getMinimumPoint();
			BlockVector3 offset = min.subtract(origin);
			int width = region.getWidth();
			int height = region.getHeight();
			int length = region.getLength();
			int volume = width * height * length;

			if (volume > MAX_VOLUME) {
				throw new IllegalArgumentException("Schematic too large.");
			}

			CompoundTag schematic = new CompoundTag("Schematic");
			schematic.putShort("Width", (short) width);
			schematic.putShort("Height", (short) height);
			schematic.putShort("Length", (short) length);
			schematic.putString("Materials", "Alpha");
			schematic.putString("Platform", PLATFORM);
			schematic.putInt("WEOffsetX", offset.getBlockX());
			schematic.putInt("WEOffsetY", offset.getBlockY());
			schematic.putInt("WEOffsetZ", offset.getBlockZ());
			schematic.putInt("WEOriginX", min.getBlockX());
			schematic.putInt("WEOriginY", min.getBlockY());
			schematic.putInt("WEOriginZ", min.getBlockZ());

			ListTag<CompoundTag> tileEntities = new ListTag<CompoundTag>();
			ListTag<CompoundTag> entities = new ListTag<CompoundTag>();

			ByteArrayOutputStream blockBuffer = new ByteArrayOutputStream(volume);
			ByteArrayOutputStream dataBuffer = new ByteArrayOutputStream(volume);

			for (int y = 0; y < height; y++) {
				int y0 = min.getBlockY() + y;
				for (int z = 0; z < length; z++) {
					int z0 = min.getBlockZ() + z;
					for (int x = 0; x < width; x++) {
						int x0 = min.getBlockX() + x;
						final BlockVector3 point = BlockVector3.at(x0, y0, z0);
						final BaseBlock block = clipboard.getFullBlock(point);
						final String namespace = block.getBlockType().toString();

						if (namespace.equals("minecraft:air")) {
							blockBuffer.write((byte) 0);
							dataBuffer.write((byte) 0);

							continue;
						}

						// Convert the properties from FAWE's HashMap implementation to mojang's CompoundTag
						CompoundTag properties = new CompoundTag();
						Map<Property<?>, Object> blockStates = block.getStates();
						for (Map.Entry<Property<?>, Object> entry : blockStates.entrySet()) {
							properties.putString(entry.getKey().getName(), entry.getValue().toString().toLowerCase());
						}

						int combined = /*NUMCAConverter.processBlock(namespace, properties);*/0;
						int id = combined >> 4;

						blockBuffer.write((byte) id);
						dataBuffer.write((byte) (combined & 0xF));

						if (MCAUtil.TILE_BLOCKS.contains(id)) {
							CompoundTag tile = new CompoundTag();

							if (id == 63 || id == 68) {
								// Only process the data from the old tile entity if it's a sign
								if (block.getNbtData() == null) {
									continue;
								}

								Map<String, Tag> values = new HashMap<>(block.getNbtData().getValue());

								tile.putString("id", "Sign");
								StringJoiner text = new StringJoiner("\n");
								for (int k = 1; k <= 4; ++k) {
									String line = (String) values.get("Text" + k).getValue();

									if (line.indexOf("{\"text\":\"") == 0) {
										text.add(line.substring(("{\"text\":\"").length(), line.length() - 2));
									} else if (line.indexOf("{\"extra\":[{\"text\":\"") == 0) {
										text.add(line.substring(("{\"extra\":[{\"text\":\"").length(), line.indexOf("\"}],\"text\":\"\"}")));
									} else {
										text.add(line);
									}
								}
								tile.putString("Text", text.toString());
								tile.putString("Creator", new String());
							} else {
								tile = OldMCAConverter.convertTile(id, namespace, properties);
							}

							tile.putInt("x", x);
							tile.putInt("y", y);
							tile.putInt("z", z);

							tileEntities.add(tile);
						}
					}
				}
			}
			final byte[] blocks = blockBuffer.toByteArray();
			final byte[] blockData = dataBuffer.toByteArray();
			try {
				blockBuffer.close();
				dataBuffer.close();
			} catch (IOException e) {
			}

			final List<? extends Entity> clipboardEntities = clipboard.getEntities();
			for (Entity entity : clipboardEntities) {
				BaseEntity state = entity.getState();
				if (state == null) {
					continue;
				}

				String entityId = state.getType().getId();
				if (entityId.equals("minecraft:item_frame")) {
					final Vector3 location = entity.getLocation().toVector();
					int x = location.getBlockX() - min.getBlockX();
					int y = location.getBlockY() - min.getBlockY();
					int z = location.getBlockZ() - min.getBlockZ();
					Map<String, Tag> values = new HashMap<>(state.getNbtData().getValue());

					// Item frame block
					int index = getClipboardIndex(x, y, z, width, length);
					byte facingMeta = RegionFormat.metabit((byte) values.get("Facing").getValue());
					blocks[index] = (byte) 199;
					blockData[index] = facingMeta;

					// Tile entity
					Tag itemTag = values.get("Item");
					if (itemTag != null) {
						CompoundTag itemFrame = new CompoundTag();
						CompoundTag item = new CompoundTag();
						int itemCombined = OldMCAConverter.getItem((String) ((Map<String, Tag>) itemTag.getValue()).get("id").getValue());
						short itemId = (short) (itemCombined >> 4);
						short itemMeta = (short) (itemCombined & 0xF);

						// Item frame is a tile entity on Bedrock
						itemFrame.putString("id", "ItemFrame");
						itemFrame.putInt("x", x);
						itemFrame.putInt("y", y);
						itemFrame.putInt("z", z);
						itemFrame.putFloat("ItemDropChance", (float) 1.0);

						item.putShort("id", itemId);
						item.putShort("Damage", itemMeta);
						item.putByte("Count", (byte) 1);
						itemFrame.putCompound("Item", item);

						Tag itemRotation = values.get("ItemRotation");
						if (itemRotation == null) {
							itemFrame.putByte("ItemRotation", (byte) 0);
						} else {
							itemFrame.putByte("ItemRotation", (byte) itemRotation.getValue());
						}
						tileEntities.add(itemFrame);
					}
				} else if (entityId.equals("minecraft:painting")) {
					final Vector3 location = entity.getLocation().toVector();
					Map<String, Tag> values = new HashMap<>(state.getNbtData().getValue());

					CompoundTag painting = MCAUtil.newBaseEntity("Painting");

					int x = location.getBlockX();
					int y = location.getBlockY();
					int z = location.getBlockZ();
					painting.putInt("TIleX", x);
					painting.putInt("TIleY", y);
					painting.putInt("TIleZ", z);
					
					String motif = (String) values.get("Motive").getValue();
					String newMotif = OldMCAConverter.PAINTING_MOTIFS.get(motif);
					if (newMotif != null) {
						painting.putString("Motive", newMotif);
					} else {
						motif = motif.substring(motif.indexOf(":") + 1);
						painting.putString("Motive", motif.substring(0, 1).toUpperCase() + motif.substring(1));
					}

					byte facing = painting.getByte("Facing");
					float pitch = 0;
					switch (facing) {
					case 0: pitch = 270; break;
					case 1: pitch = 360; break;	
					case 2: pitch = 180; break;
					case 3: pitch = 90; break;
					}
					painting.putByte("Direction", facing);
					
					ListTag rot = new ListTag();
					rot.add(new FloatTag("", pitch));
					rot.add(new FloatTag("", 0));
					painting.put("Rotation", rot);

					entities.add(painting);
				}
			}

			schematic.putByteArray("Blocks", blocks);
			schematic.putByteArray("Data", blockData);
			schematic.put("TileEntities", tileEntities);
			schematic.put("Entities", entities);

			done.value = schematic;
			TaskManager.IMP.sync(done);
		});
	}
	
	private final static int linkLength = "https://file.io/ijiknU2QqWde".length();
	private final static int linkKeyLength = "\"link\":\"".length();

	@SuppressWarnings("deprecation")
	public static void createSchematic(Player player) {
		final BukkitPlayer fawePlayer = BukkitAdapter.adapt(player);
		try {
			final Clipboard clipboard = fawePlayer.getSession().getClipboard().getClipboard();
			final UUID uuid = player.getUniqueId();

			write(clipboard, new RunnableVal<CompoundTag>() {
				@Override
				public void run(CompoundTag tag) {
					String response = null;
					try {
						response = save(uuid, tag);
					} catch (IOException e) {
						response = e.toString();
					}

					int index = response.indexOf("\"link\":");
					if (index != -1) {
						int start = index + linkKeyLength;
						final String link = response.substring(start, start + linkLength);

						if (ConverterPlugin.isGeyserLoaded) {
							// Bedrock players can't open chat links. Send them a form instead
							if (GeyserHook.trySendLinkForm(player, link)) {
								return;
							}
						}
						// PC players can open links from chat
						player.sendMessage(ChatColor.AQUA + "Schematic created. Download:");
						player.sendMessage(ChatColor.GRAY + link);
					} else {
						player.sendMessage(ChatColor.RED + "An error occured while trying to export your schematic:");
						player.sendMessage(ChatColor.RED + response);
					}
				}
			});
		} catch (EmptyClipboardException e) {
			player.sendMessage(ChatColor.RED + "You must do //copy first.");
		} catch (IllegalArgumentException e) {
			player.sendMessage(ChatColor.RED + e.getMessage());
		}
	}

	private static int getClipboardIndex(int x, int y, int z, int width, int length) {
		return y * width * length + z * width + x;
	}
}
