package me.ppopovabg.converter.format;

import static me.ppopovabg.converter.format.RegionFormat.*;
import static org.iq80.leveldb.impl.Iq80DBFactory.factory;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;
import org.iq80.leveldb.CompressionType;
import org.iq80.leveldb.DB;
import org.iq80.leveldb.Options;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mojang.nbt.NbtIo;
import com.mojang.nbt.RegionFile;

import nl.itslars.mcpenbt.NBTUtil;
import nl.itslars.mcpenbt.tags.CompoundTag;
import nl.itslars.mcpenbt.tags.FloatTag;
import nl.itslars.mcpenbt.tags.StringTag;
import nl.itslars.mcpenbt.tags.Tag;
import nl.itslars.mcpenbt.tags.IntTag;
import nl.itslars.mcpenbt.tags.ListTag;
import nl.itslars.mcpenbt.tags.ByteTag;

public class LevelDBWriter extends RegionConverter {
	private static final String outputFolder = "db";
	private static final Map<String, Map> BLOCK_STATES = new HashMap<>();
	
	public LevelDBWriter(String worldname) {
		super(worldname, "region", outputFolder, ".mca", WorldFormat.LEVELDB);
	}

	@Override
	public void convertLevel(final File regionDir, final String outPath) {
		final File[] regions = regionDir.listFiles(f -> f.isFile() && f.getName().endsWith(inputFileExtension));
		final int regionsCount = regions.length;
		int regionsProcessed = 0;
		
		try {
			Options options = new Options()
					.createIfMissing(true)
					.compressionType(CompressionType.ZLIB_RAW)
					.blockSize(64 * 1024);

			final DB db = factory.open(new File(worldPath, outputFolder), options);

			for (File region : regions) {
				if (!isRunning) {
					System.out.println(ChatColor.GRAY + "Conversion has been forcibly stopped.");
					break;
				}

				log("Processing " + ChatColor.GRAY + region.getName() + " [" + ++regionsProcessed + "/"
						+ regionsCount + "]");

				RegionFile regionSource = new RegionFile(region);

				for (int cx = 0; cx < 32; cx++) {
					for (int cz = 0; cz < 32; cz++) {
						if (regionSource.hasChunk(cx, cz)) {
							try {
								DataInputStream regionChunkInputStream = regionSource.getChunkDataInputStream(cx, cz);
								if (regionChunkInputStream == null) {
									throw new NullPointerException("regionChunkInputStream was null");
								}
								
								com.mojang.nbt.CompoundTag root = NbtIo.read(regionChunkInputStream);
								regionChunkInputStream.close();

								int dataVersion = root.getInt("DataVersion");
								if (verifyDataVersion(dataVersion) == false) {
									continue;
								}
								
								if (!root.contains("Level")) {
									throw new RuntimeException("No Level tag present");
								}

								writeSections(root.getCompound("Level"), dataVersion, db);
								
							} catch (Exception e) {
								log(ChatColor.GRAY + "An error occurred while trying to read chunk (" + cx + ", "
										+ cz + "): " + e);
								e.printStackTrace();
							}
						}
					}
				}
				log("Blocks in region affected: " + ChatColor.AQUA + blocksConvertedInRegionCount);

				regionSource.close();
			}

			db.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void writeSections(com.mojang.nbt.CompoundTag anvilChunk, int dataVersion, final DB db) throws IOException {
		int chunkX = anvilChunk.getInt("xPos");
		int chunkZ = anvilChunk.getInt("zPos");

		if (db.get(getLevelDBKey(chunkX, chunkZ, (byte) 0, LDB_SUBCHUNK_PREFIX)) == null) {
			// Don't create new chunks
			return;
		}

		com.mojang.nbt.ListTag anvilSections = anvilChunk.getList("Sections");
		com.mojang.nbt.ListTag tileEntities = anvilChunk.getList("TileEntities");
		List<CompoundTag> chunkBlockEntities = new ArrayList<>();
		
		for (int subChunkHeight = 0; subChunkHeight < anvilSections.size(); subChunkHeight++) {
			com.mojang.nbt.CompoundTag anvilSection = (com.mojang.nbt.CompoundTag) anvilSections.get(subChunkHeight);
			com.mojang.nbt.ListTag anvilPalette = anvilSection.getList("Palette");
			List blockPalette = new ArrayList(), waterloggingPalette = new ArrayList();
			long[] anvilBlockStates = anvilSection.getLongArray("BlockStates");
			
			if (anvilPalette.size() == 0) {
				blockPalette.add(new CompoundTag("", Arrays.asList(new StringTag("name", "minecraft:air"))));
				anvilBlockStates = new long[256];
				Arrays.fill(anvilBlockStates, 0L);
			}
			
			Set<Integer> waterloggingIndices = new HashSet<>();
			Map<Integer, CompoundTag> blockEntityPalette = new HashMap<>();

			for (int paletteIndex = 0; paletteIndex < anvilPalette.size(); paletteIndex++) {
				com.mojang.nbt.CompoundTag anvilBlock = (com.mojang.nbt.CompoundTag) anvilPalette.get(paletteIndex);
				com.mojang.nbt.CompoundTag blockProperties = anvilBlock.getCompound("Properties");
				CompoundTag ldbBlock = translateBlock(anvilBlock);
				
				if (blockProperties != null) {
					com.mojang.nbt.Tag waterlogged = blockProperties.get("waterlogged");
					
					if (waterlogged instanceof com.mojang.nbt.ByteTag && blockProperties.getByte("waterlogged") == 1
					|| waterlogged instanceof com.mojang.nbt.StringTag && blockProperties.getString("waterlogged").equals("true")) {
						waterloggingIndices.add(paletteIndex);
					}
				}
				
				blockPalette.add(ldbBlock);
				
				CompoundTag tile = createBlockEntity(anvilBlock.getString("Name"), null);
				if (tile != null) {
					blockEntityPalette.put(paletteIndex, tile);
				}
			}

			short[] blockStates = new short[BLOCK_ARR_SIZE];
			short[] waterloggingStates = null;

			int storageCount = waterloggingIndices.size() > 0 ? 2 : 1;
			if (storageCount == 2) {
				waterloggingStates = new short[BLOCK_ARR_SIZE];
				waterloggingPalette.add(new CompoundTag("", Arrays.asList(new StringTag("name", "minecraft:air"))));
				waterloggingPalette.add(new CompoundTag("", Arrays.asList(new StringTag("name", "minecraft:water"))));
			}

			for (int y = 0; y < 16; y++) {
				for (int z = 0; z < 16; z++) {
					for (int x = 0; x < 16; x++) {
						int blockstateIndex = getLevelDBIndex(x, y, z);
						int paletteIndex = getPaletteIndex(dataVersion,
								getBlockstateIndex(x, y, z), anvilBlockStates);
						
						blockStates[blockstateIndex] = (short) paletteIndex;
						
						if (storageCount == 2) {
							if (waterloggingIndices.contains(paletteIndex)) {
								waterloggingStates[blockstateIndex] = 1;
							} else {
								waterloggingStates[blockstateIndex] = 0;
							}
						}
						
						CompoundTag blockEntity = blockEntityPalette.get(paletteIndex);
						if (blockEntity != null) {
							List data = new ArrayList<>(blockEntity.getElements());
							data.add(new IntTag("x", chunkX * 16 + x));
							int entitySection = subChunkHeight - 1;
							data.add(new IntTag("y", (entitySection * 16 + y - 1)));
							data.add(new IntTag("z", chunkZ * 16 + z));
							chunkBlockEntities.add(new CompoundTag("", data));
							if (blockEntity.getByName("id").get().getAsString().getValue().equals("Banner")) {
								System.out.println((chunkX * 16 + x) + " " + (entitySection * 16 + y) + " " + (chunkZ * 16 + z) + " " + blockEntity.getByName("Base").get().getAsInt().getValue());
							}
						}
					}
				}
			}
			
			blocksConvertedInRegionCount += blockStates.length;

			try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
				outputStream.write(8);
				outputStream.write(storageCount);

				addBlockStorageToOutputStream(outputStream, blockPalette, blockStates);
				if (storageCount == 2) {
	                addBlockStorageToOutputStream(outputStream, waterloggingPalette, waterloggingStates);
	            }

				byte[] value = outputStream.toByteArray();
				byte[] levelDBKey = getLevelDBKey(chunkX, chunkZ, anvilSection.getByte("Y"), LDB_SUBCHUNK_PREFIX);
				db.put(levelDBKey, value);
			}
		}

		try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			for (int i=0; i < tileEntities.size(); i++) {
				com.mojang.nbt.CompoundTag anvilTile = (com.mojang.nbt.CompoundTag) tileEntities.get(i);
				CompoundTag ldbTile = translateTile(anvilTile);
				outputStream.write(NBTUtil.write(ldbTile));
			}

			for (CompoundTag tile : chunkBlockEntities) {
				outputStream.write(NBTUtil.write(tile));
			}

			byte[] tilesKey = getLevelDBKey(chunkX, chunkZ, LDB_TILES_PREFIX);
			db.put(tilesKey, outputStream.toByteArray());
		}
	}
	
	private static CompoundTag createBlockEntity(String namespace, com.mojang.nbt.CompoundTag properties) {
		List<Tag> data = new ArrayList<>();
		String tileId = null;
		
		if (namespace.contains("_banner")) {
			String color = namespace.replaceAll("minecraft:|_banner|_wall", "");
			int base = 0;

			switch (color) {
			case "red": base = 1; break;
			case "green": base = 2; break;
			case "brown": base = 3; break;
			case "blue": base = 4; break;
			case "purple": base = 5; break;
			case "cyan": base = 6; break;
			case "light_gray": base = 7; break;
			case "gray": base = 8; break;
			case "pink": base = 9; break;
			case "lime": base = 10; break;
			case "yellow": base = 11; break;
			case "light_blue": base = 12; break;
			case "magenta": base = 13; break;
			case "orange": base = 14; break;
			case "white": base = 15; break;
			}

			data.add(new IntTag("Base", base));
			data.add(new IntTag("Type", 0));
			tileId = "Banner";
		} else if (namespace.contains("_skull")) {
			byte skullType = 0;
			if (namespace.indexOf("wither") != -1) 		skullType = 1;
			else if (namespace.indexOf("zombie") != -1) skullType = 2;
			else if (namespace.indexOf("player") != -1) skullType = 3;
			else if (namespace.indexOf("creeper") != -1)skullType = 4;
			else if (namespace.indexOf("dragon") != -1) skullType = 5;
			if (namespace.indexOf("wall") == -1 && properties != null) {
				String rot = properties.getString("rotation");
				if (!rot.isEmpty()) {
					data.add(new ByteTag("Rot", Byte.valueOf(rot)));
				}
			}
			data.add(new ByteTag("SkullType", skullType));
			tileId = "Skull";
		} else if (namespace.contains("potted_")) {
			String flower = namespace.replaceAll("minecraft:|potted_", "");
			Map<String, String> mapped = BLOCK_STATES.get("minecraft:" + flower);
			String name = mapped == null ? flower : mapped.get("name");
			if (name != null) {
				List plantBlock = new ArrayList();
				if (mapped != null) {
					String statesStr = mapped.get("states");
					if (statesStr != null) {
						plantBlock.add(new CompoundTag("states", Arrays.asList(new StringTag(statesStr.split("=")[0], statesStr.split("=")[1]))));
					}
				}
				plantBlock.add(new StringTag("name", name));
				data.add(new CompoundTag("PlantBlock", plantBlock));
			}
			
			tileId = "FlowerPot";
		} else if (namespace.contains("_bed")) {
			byte color = 0;
			switch (namespace.replace("minecraft:", "")) {
			case "orange_bed": 	color = 1; break;
			case "magenta_bed": color = 2; break;
			case "light_blue_bed":color = 3; break;
			case "yellow_bed": 	color = 4; break;
			case "lime_bed": 	color = 5; break;
			case "pink_bed": 	color = 6; break;
			case "gray_bed": 	color = 7; break;
			case "light_gray_bed":color = 8; break;
			case "cyan_bed": 	color = 9; break;
			case "purple_bed": 	color = 10; break;
			case "blue_bed": 	color = 11; break;
			case "brown_bed": 	color = 12; break;
			case "green_bed": 	color = 13; break;
			case "red_bed": 	color = 14; break;
			case "black_bed": 	color = 15; break;
			}
			data.add(new ByteTag("color", color));
			tileId = "Bed";
		}
		
		if (tileId == null) {
			return null;
		}

		data.add(new StringTag("id", tileId));
		data.add(new ByteTag("isMovable", (byte) 1));
		
		return new CompoundTag("", data);
	}
	
	private static CompoundTag translateTile(com.mojang.nbt.CompoundTag anvilTile) {
		List data = convertAllTags(anvilTile);
		String tileId = anvilTile.getString("id").replace("minecraft:", "");

		if (tileId.contains("_")) {
			String part2 = tileId.substring(tileId.indexOf("_") + 1, tileId.length());
			tileId = tileId.substring(0, 1).toUpperCase() + tileId.substring(1, tileId.indexOf("_")) + part2.substring(0, 1).toUpperCase() + part2.substring(1);
		} else {
			tileId = tileId.substring(0, 1).toUpperCase() + tileId.substring(1);
		}
		
		CompoundTag tile = new CompoundTag("", data);
		tile.change("id", new StringTag("id", tileId));
		
		return tile;
	}
	
	private static List<Tag> convertAllTags(com.mojang.nbt.Tag nbt) {
		List data = new ArrayList();
		
		if (nbt instanceof com.mojang.nbt.CompoundTag) {
			com.mojang.nbt.CompoundTag compound = (com.mojang.nbt.CompoundTag) nbt;
			for (com.mojang.nbt.Tag tag : compound.getAllTags()) {
				String name = tag.getName();
				if (tag instanceof com.mojang.nbt.StringTag) {
					data.add(new StringTag(name, compound.getString(name)));
				} else if (tag instanceof com.mojang.nbt.IntTag) {
					data.add(new IntTag(name, compound.getInt(name)));
				} else if (tag instanceof com.mojang.nbt.FloatTag) {
					data.add(new FloatTag(name, compound.getFloat(name)));
				} else if (tag instanceof com.mojang.nbt.ByteTag) {
					data.add(new ByteTag(name, compound.getByte(name)));
				} else if (tag instanceof com.mojang.nbt.CompoundTag) {
					data.add(new CompoundTag(name, convertAllTags(compound.getCompound(name))));
				} else if (tag instanceof com.mojang.nbt.ListTag) {
					List<Tag> elems = convertAllTags(compound.get(name));
					if (elems.size() != 0) {
						data.add(new ListTag(name, elems.get(0).getType(), elems));
					}
				}
			}
		}
		
		return data;
	}

	private static CompoundTag translateBlock(final com.mojang.nbt.CompoundTag anvilBlock) {
		List block = new ArrayList(), states = new ArrayList();
		final String anvilNamespace = "minecraft:" + anvilBlock.getString("Name").replace("minecraft:", "");
		com.mojang.nbt.CompoundTag anvilProperties = anvilBlock.getCompound("Properties");
		String translatedNamespace = anvilNamespace;
	
		Map<String, String> mappedStates = BLOCK_STATES.get(anvilNamespace);
		if (mappedStates != null) {
			translatedNamespace = mappedStates.get("name");
			String translatedStatesString = mappedStates.get("states");
			if (translatedStatesString != null) {
				for (String stateString : translatedStatesString.split(";")) {
					states.add(stateFromString(stateString.split("=")[0], stateString.split("=")[1]));
				}
			}
			
			int hashcode = 0;
			for (com.mojang.nbt.Tag anvilProperty : anvilProperties.getAllTags()) {
				String propName = anvilProperty.getName();
				String propValue = null;
				if (anvilProperty instanceof com.mojang.nbt.ByteTag) {
					propValue = anvilProperties.getByte(propName) == 1 ? "true" : "false";
				} else if (anvilProperty instanceof com.mojang.nbt.StringTag) {
					propValue = anvilProperties.getString(propName);
				} else {
					propValue = String.valueOf(anvilProperties.getInt(propName));
				}
				
				if ((propValue.equals("false") && !propName.equals("lit")) || propValue.equals("none")) {
					continue;
				}
				
				int propHashcode = (propName + "=" + propValue).hashCode();
	
				translatedStatesString = mappedStates.get(propHashcode);
				if (translatedStatesString != null) {
					for (String prop : translatedStatesString.split(";")) {
						if (prop.indexOf("=") == -1) {
							translatedNamespace = prop;
						} else {
							states.add(stateFromString(prop.split("=")[0], prop.split("=")[1]));
						}
					}
				}
				hashcode ^= propHashcode;
			}
			
			if (states.size() == 0) {
				String state = mappedStates.get(hashcode);
				if (state != null) {
					for (String stateString : state.split(";")) {
						states.add(stateFromString(stateString.split("=")[0], stateString.split("=")[1]));
					}
				}
			}
		}
		
		block.add(new StringTag("name", translatedNamespace));
		block.add(new CompoundTag("states", states));

		return new CompoundTag("", block);
	}
	
	private static Tag stateFromString(String name, String value) {
		Tag blockState = null;
		
		if (value.equals("true")) {
			blockState = new ByteTag(name, (byte)1);
		} else if (value.length() <= 2 && value.matches("[0-9]+")) {
			blockState = new IntTag(name, Integer.valueOf(value));
		} else {
			blockState = new StringTag(name, value);
		}
		
		return blockState;
	}

	private static void addBlockStorageToOutputStream(ByteArrayOutputStream outputStream,
			List<CompoundTag> palette, short[] blockStates) throws IOException {
		int paletteCount = palette.size();
		int bitsPerBlock = (int) Math.max(Math.ceil(Math.log(paletteCount) / Math.log(2)), 1);
		for (int i : new byte[] { 1, 2, 3, 4, 5, 6, 8, 16 }) {
			if (i >= bitsPerBlock) {
				bitsPerBlock = i;
				break;
			}
		}

		byte storageVersion = (byte) (bitsPerBlock << 1);
		outputStream.write(storageVersion);
		int blocksPerWord = (int) Math.floor(32.0 / bitsPerBlock);
		int wordCount = (int) Math.ceil(4096.0 / blocksPerWord);

		int position = 0;
		for (int wordIndex = 0; wordIndex < wordCount; wordIndex++) {
			int word = 0;
			int index = 0;

			for (int blockIndex = blocksPerWord - 1; blockIndex >= 0; blockIndex--) {
				if (wordIndex * blocksPerWord + blockIndex < 4096) {
					int result = blockStates[position];

					word |= (result << index);

					position++;
					index += bitsPerBlock;
				}
			}

			outputStream.write((byte) word);
			outputStream.write((byte) (word >>> 8));
			outputStream.write((byte) (word >>> 16));
			outputStream.write((byte) (word >>> 24));
		}

		outputStream.write(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(palette.size()).array());
		for (CompoundTag block : palette) {
			outputStream.write(NBTUtil.write(block));
		}
	}

	@Override
	public boolean verifyDataVersion(int dataVersion) {
		boolean result = dataVersion > WorldFormat._1_12.dataVersion;
		if (result == false) {
			log("Chunk is not in new-Anvil format; skipping region. DataVersion was: " + dataVersion);
		}
		return result;
	}
	
	@Override
	protected com.mojang.nbt.CompoundTag convertChunkNBT(com.mojang.nbt.CompoundTag oldChunk, int dataVersion) {
		// Doesn't create new chunks
		return null;
	}

	@Override
	protected com.mojang.nbt.CompoundTag convertLevelNBT(com.mojang.nbt.CompoundTag oldLevel) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static void loadBlockTable(JSONParser parser, InputStream stream) throws UnsupportedEncodingException, IOException, ParseException {
		final JSONObject root = (JSONObject) parser.parse(new InputStreamReader(stream, "UTF-8"));
		root.keySet().forEach(key -> {
			Map<Object, String> data = ((Map<String, String>)root.get(key)).entrySet()
						.stream()
						.collect(Collectors.toMap(
							entry -> {
								String entryKey = entry.getKey().toString();
								if (entryKey.equals("name") || entryKey.equals("states")) return entryKey;
								
								String javaStates = entryKey.toLowerCase();
								int hashcode = 0;
								for (String propStr : javaStates.split(";"))
									hashcode ^= propStr.hashCode();
								return hashcode;
							},
							Map.Entry::getValue
						));
			BLOCK_STATES.put(key.toString(), data);
		});
		parser.reset();
	}
}
