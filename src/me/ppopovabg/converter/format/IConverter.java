package me.ppopovabg.converter.format;

import java.io.File;
import java.io.IOException;

import org.bukkit.command.CommandSender;

public interface IConverter {
	void start(final CommandSender sender);
	void convertLevel(final File inputDir, final String outPath) throws IOException;
	void stop();
	void log(String message);
	
	String getWorldName();
}