package me.ppopovabg.converter.format;

import static me.ppopovabg.converter.format.RegionFormat.*;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mojang.nbt.CompoundTag;
import com.mojang.nbt.NbtIo;
import com.mojang.nbt.RegionFile;

import me.ppopovabg.converter.ConverterPlugin;
import me.ppopovabg.converter.format.RegionFormat.WorldFormat;

import javax.annotation.Nullable;

public abstract class RegionConverter implements IConverter {
	
	interface OldDataLayer {
		int getIndex(int x, int y, int z, int yBase);
		
		default byte[] extractBlocks(final CompoundTag chunkOrSection) {
			return chunkOrSection.getByteArray("Blocks");
		}
		default byte[] extractBlockData(final CompoundTag chunkOrSection) {
			return chunkOrSection.getByteArray("Data");
		}
		default byte[] extractBlockLight(final CompoundTag chunkOrSection) {
			return chunkOrSection.getByteArray("BlockLight");
		}
		default byte[] extractSkyLight(final CompoundTag chunkOrSection) {
			return chunkOrSection.getByteArray("SkyLight");
		}
		default boolean hasFixedSectionSize() {
			return false;
		}
	}
	
	public static class MCAPMDataLayer implements OldDataLayer {
		@Override public int getIndex(int x, int y, int z, int yBase) {
			return x * 256 + z * 16 + y;
		}
		@Override public byte[] extractBlockLight(CompoundTag section) {
			return reorderNibbleArray(section.getByteArray("BlockLight"));
		}
		@Override public byte[] extractSkyLight(CompoundTag section) {
			return reorderNibbleArray(section.getByteArray("SkyLight"));
		}
	}
	
	public static class MCRDataLayer implements OldDataLayer {
		@Override public int getIndex(int x, int y, int z, int yBase) {
			return getMCRIndex(x, y, z, yBase);
		}
		@Override public boolean hasFixedSectionSize() {
			return true;
		}
		@Override public byte[] extractBlockLight(@Nullable CompoundTag __) {
			byte[] light = new byte[NIBBLE_ARR_SIZE];
			Arrays.fill(light, (byte) 0xFF);
			return light;
		}
		@Override public byte[] extractSkyLight(@Nullable CompoundTag __) {
			byte[] light = new byte[NIBBLE_ARR_SIZE];
			Arrays.fill(light, (byte) 0xFF);
			return light;
		}
	}
	
	public static class OldMCADataLayer implements OldDataLayer {
		@Override public int getIndex(int x, int y, int z, int yBase) {
			return getBlockstateIndex(x, y, z);
		}
	}

	protected final String worldName;
	protected final String worldPath;
	protected final String inputFolder;
	protected final String outputFolder;
	protected final String inputFileExtension;
	protected final String outputFileExtension;
	protected final WorldFormat outputFormat;
	protected final Map<File, RegionFile> regionFileCache = new HashMap<>();
	protected final Set<String> unknownBlocks = new HashSet<>();

	protected boolean isRunning = true;
	protected int blocksConvertedInRegionCount = 0;
	protected CommandSender commandSender = null;
	
	public RegionConverter(String world_name,
						String region_folder,
						String output_folder,
						String file_extension_in,
						WorldFormat outputFormat) {
		this.worldName = world_name;
		this.worldPath = Bukkit.getWorldContainer() + "/" + world_name;
		this.inputFolder = region_folder;
		this.outputFolder = output_folder;
		this.inputFileExtension = file_extension_in;
		this.outputFileExtension = outputFormat.ext;
		this.outputFormat = outputFormat;
	}

	protected abstract CompoundTag convertLevelNBT(final CompoundTag oldLevel);
	protected abstract CompoundTag convertChunkNBT(final CompoundTag oldChunk, int dataVersion);
	protected abstract boolean verifyDataVersion(int dataVersion);
	
	public void start(CommandSender sender) {
		this.commandSender = sender;

		log("Using converter: " + getClass().getName());
		
		File regionDir = new File(worldPath, inputFolder);
		
		if (!regionDir.isDirectory()) {
			log(ChatColor.RED + "Folder '" + regionDir + "' does not exist in the world directory.");
			stop();
			
			return;
		}
		
		String outPath = worldPath + File.separator + outputFolder;
		File guard = new File(worldPath, "conversion in progress");
		try {
			new File(outPath).mkdir();
			guard.createNewFile();
			guard.deleteOnExit();
		} catch (IOException e) {}

		log("Conversion initiated for world '" + worldName + "'. Regions will be written to /" + outputFolder);
		log("Output format: " + ChatColor.AQUA + outputFormat.name);

		convertLevel(regionDir, outPath);
		convertWorldData();
		
		log(ChatColor.GREEN + "Conversion completed.");
		
		guard.delete();
		stop();
	}
	
	public void convertLevel(final File regionDir, final String outPath) {
		final File[] regions = regionDir.listFiles(f -> f.isFile() && f.getName().endsWith(inputFileExtension));
		final int regionsCount = regions.length;
		int regionsProcessed = 0;
		
		for (File region : regions) {
			if (!isRunning) {
				System.out.println(ChatColor.GRAY + "Conversion has been forcibly stopped.");
				return;
			}
			
			log("Processing " + ChatColor.GRAY + region.getName() + " [" + ++regionsProcessed + "/" + regionsCount + "]");

			RegionFile regionSource = new RegionFile(region);
			File regionOut = new File(outPath, region.getName().substring(0,region.getName().length()-inputFileExtension.length()) + outputFileExtension);
			RegionFile regionDest = new RegionFile(regionOut);
			
			blocksConvertedInRegionCount = 0;
			boolean allEmpty = true;
			for (int cx = 0; cx < 32; cx++) {
				for (int cz = 0; cz < 32; cz++) {
					if (regionSource.hasChunk(cx, cz)) {
						try {
							DataInputStream regionChunkInputStream = regionSource.getChunkDataInputStream(cx, cz);
							CompoundTag root = NbtIo.read(regionChunkInputStream);
							regionChunkInputStream.close();

							if (verifyDataVersion(root.getInt("DataVersion")) == false) {
								if (cz == 31) {
									regionSource.close();
								}
								continue;
							}

							CompoundTag tag = convertChunkNBT(root.getCompound("Level"), root.getInt("DataVersion"));
							if (tag != null) {
								DataOutputStream chunkDataOutputStream = regionDest.getChunkDataOutputStream(cx, cz);
								NbtIo.write(tag, chunkDataOutputStream);
								chunkDataOutputStream.close();
								allEmpty = false;
							}
							
						} catch (Exception e) {
							log(ChatColor.GRAY + "An error occurred while trying to read chunk (" + cx + ", " + cz + "): " + e);
							e.printStackTrace();
						}
					}
				}
			}

			try {
				regionSource.close();
				regionDest.close();
			} catch (IOException e) {}
			
			if (allEmpty) {
				regionOut.delete();
			}
			
			log("Blocks in region affected: " + ChatColor.AQUA + blocksConvertedInRegionCount);
		}
		
		if (unknownBlocks.size() > 0) {
			log(ChatColor.GRAY + "The following blocks were not found in the block ID table: (" + String.join(", ", unknownBlocks) + ")");
		}
	}

	protected void convertWorldData() {
		log("Converting level.dat...");
		
		File level = new File(worldPath, "level.dat");
		if (level.isFile()) {
			try {
				String backupFile = worldPath + File.separator + "backup_level.dat";
				if (!Files.exists(Paths.get(backupFile))) {
					Files.copy(Paths.get(level.getPath()), Paths.get(backupFile));
				}

				DataInputStream in = new DataInputStream(new FileInputStream(backupFile));
				CompoundTag oldLevel = NbtIo.readCompressed(in);
				in.close();

				CompoundTag newLevel = convertLevelNBT(oldLevel.getCompound("Data"));
				BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(level));
				NbtIo.writeCompressed(newLevel, out);
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void stop() {
		this.isRunning = false;
		
		clearFileCache();
		
		ConverterPlugin.instance.stopProcessor(this);
	}

	protected void clearFileCache() {
		Iterator<?> it = regionFileCache.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<File, RegionFile> pair = (Map.Entry<File, RegionFile>) it.next();
			try {
				pair.getValue().close();
			} catch (IOException e) {}
			it.remove();
		}
	}
	
	public void log(String message) {
		String formatted = String.format("[%s] %s", worldName, message);
		
		if (commandSender instanceof Player) {
			commandSender.sendMessage(formatted);
		}

		Bukkit.getLogger().info(formatted);
	}
	
	public String getWorldName() {
		return this.worldName;
	}
}
