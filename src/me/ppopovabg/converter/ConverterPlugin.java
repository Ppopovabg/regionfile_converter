package me.ppopovabg.converter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.geysermc.connector.common.ChatColor;
import org.json.simple.parser.JSONParser;
import org.monstercraft.info.plugin.utils.JarUtils;

import me.ppopovabg.converter.format.*;

public class ConverterPlugin extends JavaPlugin implements Listener {
	private final Map<String, IConverter> currentProcessors = new ConcurrentHashMap<>();
	
	public static boolean isFaweLoaded = false, isGeyserLoaded = false;
	public static ConverterPlugin instance = null;

	@Override
	public void onEnable() {
		try {
			final File[] libs = new File[] {
                new File(getDataFolder(), "commons-compress-1.20.jar"),
                new File(getDataFolder(), "leveldb-paletted.jar")};
			
	        for (final File lib : libs) {
	            if (!lib.exists()) {
	                JarUtils.extractFromJar(lib.getName(), lib.getAbsolutePath());
	            }
	            if (lib.exists()) {
	            	addClassPath(JarUtils.getJarUrl(lib));
	            }
	        }
	        
	        final JSONParser parser = new JSONParser();
			MCAConverter.loadBlockTable(parser, saveAndLoadResource("bedrock2java_blocks.json"));
			MCAConverter.loadItemTable(parser, saveAndLoadResource("bedrock2java_items.json"));
			OldMCAConverter.loadBlockTable(saveAndLoadResource("java2bedrock_blocks.properties"));
			OldMCAConverter.loadOldBlockTable(parser, saveAndLoadResource("1.12_blocks.json"));
			OldMCAConverter.loadItemTable(parser, saveAndLoadResource("java2bedrock_items.json"));
			LevelDBReader.loadBlockTable(parser, saveAndLoadResource("leveldb2java_states.json"));
			LevelDBWriter.loadBlockTable(parser, saveAndLoadResource("java2leveldb_states.json"));
			MCAUtil.loadSignCharList(parser, getResource("sign_character_widths.json"));
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			getLogger().warning("Error: invalid JSON");
			e.printStackTrace();
		}
	
		getCommand("convert").setExecutor(new ConvertCommand(this));

		if (getServer().getPluginManager().getPlugin("FastAsyncWorldEdit") != null) {
			isFaweLoaded = true;
		}
		if (getServer().getPluginManager().getPlugin("Geyser-Spigot") != null) {
			isGeyserLoaded = true;
		}

		getServer().getPluginManager().registerEvents(this, this);

		instance = this;
	}
	
	// No longer works for JDK > 8
	private static void addClassPath(final URL url) throws IOException {
        final URLClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
        final Class<URLClassLoader> sysclass = URLClassLoader.class;
        try {
            final Method method = sysclass.getDeclaredMethod("addURL",  new Class[] { URL.class });
            method.setAccessible(true);
            method.invoke(sysloader, new Object[] { url });
        } catch (final Throwable t) {
            t.printStackTrace();
            throw new IOException("Error adding " + url + " to system classloader");
        }
    }
	
	public<T extends IConverter> T registerConverter(Class<T> clazz, Object[] args) throws Exception {
		return clazz.getDeclaredConstructor(clazz).newInstance(args);
	}
	
	private boolean debugEnabled = false;
	
	public InputStream saveAndLoadResource(String filename) throws FileNotFoundException {
		if (debugEnabled) return getResource(filename);
		
		File dataFile = new File(getDataFolder(), filename);
		if (dataFile.exists()) {
	        return new FileInputStream(dataFile);
		} else {
	        saveResource(filename, false);
	        return getResource(filename);
		}
	}
    
	@Override
	public void onDisable() {
		stopAllProcesses();
	}

	@EventHandler
	public void on(PlayerCommandPreprocessEvent event) {
		if (!isFaweLoaded) {
			return;
		}

		// Preprocess FAWE's //schematic command
		int index = event.getMessage().indexOf("schem");
		if (index == 1 || index == 2) {
			String[] args = event.getMessage().split(" ");
			if (args.length > 1) {
				if (args[1].equalsIgnoreCase("convert")) {
					//NukkitSchemConverter.createSchematic(event.getPlayer());

					// Preprocessed
					event.setCancelled(true);
				}
			}
		}
	}
	
	public boolean isConversionStarted(String worldname) {
		return currentProcessors.containsKey(worldname);
	}
	
	public void stopProcessor(IConverter processor) {
		currentProcessors.remove(processor.getWorldName());
	}
	
	public void stopAllProcesses() {
		for (Map.Entry<?,?> pair : currentProcessors.entrySet()) {
			IConverter processor = (IConverter) pair.getValue();
			processor.stop();
		}
		
		getServer().getScheduler().cancelTasks(this);
		currentProcessors.clear();
	}

	public void startProcessor(CommandSender sender, String worldname, IConverter processor) {
		if (Bukkit.getWorld(worldname) != null) {
			sender.sendMessage(ChatColor.RED + "You must unload world '" + worldname + "' before initiating conversion.");
			return;
		}

		currentProcessors.put(worldname, processor);

		Bukkit.getScheduler().runTaskAsynchronously(this, () -> 
			processor.start(sender)
		);
	}
}