package me.ppopovabg.converter;

import org.bukkit.entity.Player;
import org.geysermc.common.window.CustomFormBuilder;
import org.geysermc.common.window.CustomFormWindow;
import org.geysermc.common.window.component.InputComponent;
import org.geysermc.connector.GeyserConnector;
import org.geysermc.connector.network.session.GeyserSession;
import org.geysermc.floodgate.FloodgateAPI;

import com.nukkitx.protocol.bedrock.packet.ModalFormRequestPacket;

public final class GeyserHook {
	private static final String USERNAME_PREFIX = "_";
	
	public static boolean trySendLinkForm(Player player, String link) {
		if (FloodgateAPI.isBedrockPlayer(player.getUniqueId())) {
			for (GeyserSession session : GeyserConnector.getInstance().getPlayers()) {
				if (player.getName().equals(USERNAME_PREFIX + session.getName())) {
					CustomFormWindow window = new CustomFormBuilder("Schematic created")
							.addComponent(new InputComponent("Download link:", "", link))
							.build();

					ModalFormRequestPacket modalFormRequestPacket = new ModalFormRequestPacket();
					modalFormRequestPacket.setFormData(window.getJSONData());
					modalFormRequestPacket.setFormId((int) ((Math.random() * (2000 - 1001)) + 1001));
					session.sendUpstreamPacket(modalFormRequestPacket);

					return true;
				}
			}
		}
		return false;
	}

}
