package me.ppopovabg.converter;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.ppopovabg.converter.format.*;
import me.ppopovabg.converter.format.RegionConverter.MCAPMDataLayer;
import me.ppopovabg.converter.format.RegionConverter.MCRDataLayer;
import me.ppopovabg.converter.format.RegionConverter.OldMCADataLayer;
import me.ppopovabg.converter.format.RegionFormat.WorldFormat;

public final class ConvertCommand implements CommandExecutor {
	private final ConverterPlugin plugin;
	
	public ConvertCommand(ConverterPlugin plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 0) {
			return sendUsage(sender);
		}
		
		String subcommand = args[0].toLowerCase();
		if (args.length == 1) {
			switch (subcommand) {
			case "abort":
			case "stop":
			case "cancel":
				plugin.stopAllProcesses();
				break;
			default:
				return sendUsage(sender);
			}
		} else {
			String worldname = args[0];
			IConverter processor = null;

			if (plugin.isConversionStarted(worldname)) {
				sender.sendMessage(ChatColor.RED + "Conversion has already been initiated for '" + worldname + "'.");
				
				return true;
			}
			
			WorldFormat inputFormat = RegionFormat.guessWorldFormat(worldname);
			switch (inputFormat) {
			case ANVIL:
				String outputFormat = args[1].toLowerCase();
				switch (outputFormat) {
				case "java":
					processor = new MCAConverter(OldMCADataLayer.class, worldname, inputFormat);
					break;
				case "bedrock":
					processor = new OldMCAConverter(worldname, WorldFormat.NUKKIT);
					break;
				case "1.12":
					processor = new OldMCAConverter(worldname, WorldFormat._1_12);
					break;
				case "1.8":
					processor = new OldMCAConverter(worldname, WorldFormat._1_8);
					break;
				case "leveldb":
					processor = new LevelDBWriter(worldname);
					break;
				default:
					sender.sendMessage(ChatColor.RED + "Unknown output world format: " + outputFormat);
					return true;
				}
				break;
			case PMANVIL:
				processor = new MCAConverter(MCAPMDataLayer.class, worldname, inputFormat);
				break;
			case MCREGION:
				processor = new MCAConverter(MCRDataLayer.class, worldname, inputFormat);
				break;
			case LEVELDB:
				processor = new LevelDBReader(worldname);
				break;
			default: 
				sender.sendMessage(ChatColor.RED + "World '" + worldname + "' is in an unknown format or the region folder is empty.");
				
				return true;
			}
			
			plugin.startProcessor(sender, worldname, processor);
		}
		return true;
	}

	private static boolean sendUsage(CommandSender sender) {
		sender.sendMessage("§4/convert <world name> <world format>: §7Convert the specified world to a world format.");
		return true;
	}
}
